import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import "./assets/css/app.css";
import "bootstrap/dist/css/bootstrap.css";
import './assets/css/now-ui-dashboard.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter} from 'react-router-dom';
import {createStore} from 'redux'
import reducer from './Reducers'
import {Provider} from "react-redux";

//https://github.com/facebook/immutable-js

const store = createStore(reducer, {}, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

