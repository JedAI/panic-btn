import React from 'react';
import {Route, Switch} from 'react-router-dom';
import CltEdit from "./edit";
import CltList from "./list";

const Clt = () => (
    <Switch>
        <Route exact path='/clt' component={CltList}/>
        <Route path='/clt/:id' component={CltEdit}/>
    </Switch>
);
export default Clt;