import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class CltList extends Component {

    //todo fetch from database
    constructor() {
        super(...arguments);
        this.state = {
            clts: [
                {id: 'a', nm1: 'Darwin', nm2: 'Alejandro', ap1: "Correa", ap2: "panchi", ci: '1722590948'},
                {id: 'a', nm1: 'John', nm2: '', ap1: "Doe", ap2: "", ci: '111111111'}
            ]
        };
    }

    render() {
        return (
            <div className="App">
                <h1>Clt</h1>

                <table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>CI</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.clts.map(p => (
                            <tr key={p.id}>

                                <td>{p.nm1} {p.nm2}</td>
                                <td>{p.ap1} {p.ap2}</td>
                                <td>{p.ci}</td>
                                <td>
                                    <Link to={`/clt/${p.id}`}>Editar</Link>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default CltList;