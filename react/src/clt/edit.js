import React, {Component} from 'react';
import update from 'react-addons-update';

class CltEdit extends Component {

    //todo fetch from database
    constructor() {
        super(...arguments);
        this.state = {
            clt: {
                id: 'a', nm1: 'Darwin', nm2: 'Alejandro', ap1: "Correa", ap2: "panchi", ci: '1722590948',
                cosita: {nm1: 'xxx'}
            }
        };
    }

    /**
     * https://reactjs.org/docs/update.html
     * @param event event
     */
    onCh(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        const clt = update(this.state.clt, {[name]: {$set: value}});
        this.setState({
            clt: clt
        });
    }

    onChCosita(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        const clt = update(this.state.clt, {cosita: {[name]: {$set: value}}});
        this.setState({
            clt: clt
        });
    }

    render() {
        return (
            <div className="App">
                <h1>Clt</h1>

                <input placeholder="nombre" name="nm1" onChange={this.onCh.bind(this)} value={this.state.clt.nm1}/>
                <input placeholder="ci" name="ci" onChange={this.onCh.bind(this)} value={this.state.clt.ci}/>
                <input placeholder="nm1" name="nm1" onChange={this.onChCosita.bind(this)}
                       value={this.state.clt.cosita.nm1}/>

                {this.state.clt.ci}
            </div>
        );
    }
}

export default CltEdit;