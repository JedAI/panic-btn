import React from "react";
import axios from "axios";
import {cfg} from "../../util/General";
import {
    Collapse,
    Navbar,
    Nav,
    NavItem,
    Container
} from "reactstrap";

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            dropdownOpen: false,
            color: "transparent"
        };
        this.toggle = this.toggle.bind(this);
        this.dropdownToggle = this.dropdownToggle.bind(this);
    }

    toggle() {
        if (this.state.isOpen) {
            this.setState({
                color: "transparent"
            });
        } else {
            this.setState({
                color: "white"
            });
        }
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    dropdownToggle(e) {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    openSidebar() {
        document.documentElement.classList.toggle("nav-open");
        this.refs.sidebarToggle.classList.toggle("toggled");
    }

    // function that adds color white/transparent to the navbar on resize (this is for the collapse)
    updateColor() {
        if (window.innerWidth < 993 && this.state.isOpen) {
            this.setState({
                color: "white"
            });
        } else {
            this.setState({
                color: "transparent"
            });
        }
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateColor.bind(this));
    }

    componentDidUpdate(e) {
        if (
            window.innerWidth < 993 &&
            e.history.location.pathname !== e.location.pathname &&
            document.documentElement.className.indexOf("nav-open") !== -1
        ) {
            document.documentElement.classList.toggle("nav-open");
            this.refs.sidebarToggle.classList.toggle("toggled");
        }
    }
    
    logout() {
        axios.post(cfg.apiSec + '/logout').then(ignored => {
            // hay respuesta exitosaa desde el server, si se inicio sesión
            //lanzo accion con los datos del usuario
            this.props.do_logout();
        }).catch(error => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            // add or remove classes depending if we are on full-screen-maps page or not
            <Navbar className="navbar navbar-expand-lg navbar-absolute bg-panic">
                <Container fluid>
                    <div className="navbar-wrapper">
                        <div className="navbar-toggle">
                            <button
                                type="button"
                                ref="sidebarToggle"
                                className="navbar-toggler"
                                onClick={() => this.openSidebar()}
                            >
                            </button>
                        </div>
                    </div>
                    <Collapse
                        isOpen={this.state.isOpen}
                        navbar
                        className="justify-content-end">
                        <Nav navbar>
                            <NavItem>
                                <a href="/" onClick={this.logout} className="nav-link">
                                    <i className="now-ui-icons media-1_button-power"/> Salir
                                </a>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Container>
            </Navbar>
        );
    }
}

export default Header;
