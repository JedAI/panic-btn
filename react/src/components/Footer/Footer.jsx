import React from "react";
import PropTypes from "prop-types";

class Footer extends React.Component {
  render() {
    return (
        <footer className="footer">
            <div className="container-fluid">
                <div className="copyright" id="copyright">
                    &copy;
                    Derecho Exclusivos de
                    <a href="#"> Coro A & Imbaquingo J - Quito - Ecuador</a>.
                    {/*<a href="#"> Acosta B & Coro A - Quito - Ecuador</a>.*/}
                </div>
            </div>
        </footer>
    );
  }
}

Footer.propTypes = {
  default: PropTypes.bool,
  fluid: PropTypes.bool
};

export default Footer;
