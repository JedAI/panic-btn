const api_url = () => {
    console.log('request construct api url');
    if (window.location.hostname.indexOf('localhost') >= 0) {
        return 'http://localhost:8080';
    } else {
        return "https://panic-server.herokuapp.com"
    }
};

export const cfg = {
    api: api_url() + '/api',
    apiSec: api_url()
};

console.log(cfg);