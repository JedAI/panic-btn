import React, {Component} from 'react';
import NotificationAlert from "react-notification-alert";

class Alerts extends Component {

    constructor(props) {
        super(props);
        this.notificationAlert = React.createRef();
        this.handle_error = this.handle_error.bind(this);
        this.show_error = this.show_error.bind(this);
        this.show_info = this.show_info.bind(this);
    }

    create_ops = (type, icon, msg) => {
        return {
            place: 'tr',
            message: (
                <div>
                    <div>
                        {msg}
                    </div>
                </div>
            ),
            type: type,
            icon: icon,
            autoDismiss: 12
        };

    };

    show_error = (msg) => {
        this.notificationAlert.current.notificationAlert(this.create_ops('danger', 'fas fa-exclamation-triangle', msg));
    };

    show_info = (msg) => {
        this.notificationAlert.current.notificationAlert(this.create_ops('info', 'now-ui-icons ui-1_bell-53', msg));
    };

    handle_error = (error) => {
        console.log(error);
        console.log(JSON.stringify(error));
        var msg = (error.response && error.response.data) || false;
        if (msg) {
            msg = msg.message || msg;
            this.show_error(msg);
        } else {
            this.show_error('error desconocido');
        }
    };

    render() {
        return <NotificationAlert ref={this.notificationAlert}/>
    }
}

export default Alerts;