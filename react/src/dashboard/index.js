import React, {Component} from 'react';
import axios from "axios";
import {cfg} from "../util/General";
import Alerts from "../util/error";
import {Card, CardBody, CardHeader, CardTitle, Row, Col, Table} from "reactstrap";
import {Line} from "react-chartjs-2";
import {Statistics, CardCategory} from "../components";

const chartColor = "#FFFFFF";

// General configuration for the charts with Line gradientStroke

const gradientDashboardPanelChart = {
    layout: {
        padding: {
            left: 20,
            right: 20,
            top: 0,
            bottom: 0
        }
    },
    maintainAspectRatio: false,
    tooltips: {
        backgroundColor: "#fff",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
    },
    legend: {
        position: "bottom",
        fillStyle: "#FFF",
        display: false
    },
    scales: {
        yAxes: [
            {
                ticks: {
                    fontColor: "rgba(255,255,255,0.4)",
                    fontStyle: "bold",
                    beginAtZero: true,
                    maxTicksLimit: 5,
                    padding: 10
                },
                gridLines: {
                    drawTicks: true,
                    drawBorder: false,
                    display: true,
                    color: "rgba(255,255,255,0.1)",
                    zeroLineColor: "transparent"
                }
            }
        ],
        xAxes: [
            {
                gridLines: {
                    display: false,
                    color: "rgba(255,255,255,0.1)"
                },
                ticks: {
                    padding: 10,
                    fontColor: "rgba(255,255,255,0.4)",
                    fontStyle: "bold"
                }
            }
        ]
    }
}

class index extends Component {

    constructor() {
        super(...arguments);
        this.alert = React.createRef();
        this.state = {
            datos: {
                alertas: [],
                hombres: [],
                mujeres: [],
                recibidas: {}
            }
        }
        this.load();
    }

    load = () => {
        axios.get(cfg.api + '/resumen').then(resp => {
            this.setState({datos: resp.data});
        }).catch(error => {
            this.alert.current.handle_error(error);
        });
    }

    render() {

        const dataPanelChart = (canvas) => {
            const ctx = canvas.getContext("2d");
            var chartColor = "#FFFFFF";
            var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
            gradientStroke.addColorStop(0, "#80b6f4");
            gradientStroke.addColorStop(1, chartColor);
            var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
            gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
            gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.14)");

            return {
                labels: [
                    "ENE",
                    "FEB",
                    "MAR",
                    "ABR",
                    "MAY",
                    "JUN",
                    "JUL",
                    "AGO",
                    "SEP",
                    "OCT",
                    "NOV",
                    "DIC"
                ],
                datasets: [
                    {
                        label: "Alertas",
                        borderColor: chartColor,
                        pointBorderColor: "#FFF",
                        pointBackgroundColor: "#f96332",
                        pointHoverBorderColor: chartColor,
                        pointBorderWidth: 1,
                        pointHoverRadius: 7,
                        pointHoverBorderWidth: 2,
                        pointRadius: 5,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: this.state.datos.alertas
                    }
                ]
            };
        }

        const dataUsersChartM = (canvas) => {
            var ctx = canvas.getContext("2d");
            var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
            gradientStroke.addColorStop(0, "#80b6f4");
            gradientStroke.addColorStop(1, chartColor);
            var gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
            gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
            gradientFill.addColorStop(1, "rgba(249, 99, 59, 0.40)");
            return {
                labels: [
                    "ENE",
                    "FEB",
                    "MAR",
                    "ABR",
                    "MAY",
                    "JUN",
                    "JUL",
                    "AGO",
                    "SEP",
                    "OCT",
                    "NOV",
                    "DIC"
                ],
                datasets: [
                    {
                        label: "Alertas",
                        borderColor: "#f96332",
                        pointBorderColor: "#FFF",
                        pointBackgroundColor: "#f96332",
                        pointBorderWidth: 2,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 1,
                        pointRadius: 4,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: this.state.datos.mujeres
                    }
                ]
            };
        }

        const dataUsersChartH = (canvas) => {
            var ctx = canvas.getContext("2d");
            var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
            gradientStroke.addColorStop(0, "#80b6f4");
            gradientStroke.addColorStop(1, chartColor);
            var gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
            gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
            gradientFill.addColorStop(1, "rgba(249, 99, 59, 0.40)");
            return {
                labels: [
                    "ENE",
                    "FEB",
                    "MAR",
                    "ABR",
                    "MAY",
                    "JUN",
                    "JUL",
                    "AGO",
                    "SEP",
                    "OCT",
                    "NOV",
                    "DIC"
                ],
                datasets: [
                    {
                        label: "Alertas",
                        borderColor: "#f96332",
                        pointBorderColor: "#FFF",
                        pointBackgroundColor: "#f96332",
                        pointBorderWidth: 2,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 1,
                        pointRadius: 4,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: this.state.datos.hombres
                    }
                ]
            };
        }

        return (
            <div>
                <div className="header-panic-dashboard-tittle">
                </div>
                <div className="header-panic-dashboard-tittle">
                    <b>ALERTAS EMITIDAS</b>
                </div>
                <div className="header-panic-dashboard">
                    <Line
                        data={dataPanelChart}
                        options={gradientDashboardPanelChart}
                    />
                </div>
                <br/>
                <br/>
                <Alerts ref={this.alert}/>
                <div className="content">
                    <Row>
                        <Col xs={12} md={12}>
                            <Card className="card-stats card-raised">
                                <CardBody>
                                    <Row>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="success"
                                                icon="ui-2_chat-round"
                                                title={this.state.datos.recibidas.alertas}
                                                subtitle="Alertas Recibidas"
                                            />
                                        </Col>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="primary"
                                                icon="ui-2_chat-round"
                                                title={this.state.datos.alertasMes}
                                                subtitle="Alertas Mes"
                                            />
                                        </Col>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="info"
                                                icon="users_single-02"
                                                title={this.state.datos.recibidas.registrados}
                                                subtitle="Usuarios Registrados"
                                            />
                                        </Col>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="succes"
                                                // icon="objects_support-17"
                                                icon="sport_user-run"
                                                title={this.state.datos.recibidas.promedio + " min"}
                                                subtitle="Tiempo de Respuesta"
                                            />
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>

                            <Row>
                                <Col xs={12} md={6}>
                                    <Card className="card-chart">
                                        <CardHeader>
                                            <CardCategory>Alertas</CardCategory>
                                            <CardTitle tag="h2">Mujeres</CardTitle>
                                        </CardHeader>
                                        <CardBody>
                                            <div className="chart-area">
                                                <Line
                                                    data={dataUsersChartM}
                                                    options={gradientDashboardPanelChart}
                                                />
                                            </div>
                                            <Table responsive>
                                                {/*<tbody>{this.createTableData()}</tbody>*/}
                                            </Table>
                                        </CardBody>
                                    </Card>
                                </Col>
                                <Col xs={12} md={6}>
                                    <Card className="card-chart">
                                        <CardHeader>
                                            <CardCategory>Alertas</CardCategory>
                                            <CardTitle tag="h2">Hombres</CardTitle>
                                        </CardHeader>
                                        <CardBody>
                                            <div className="chart-area">
                                                <Line
                                                    data={dataUsersChartH}
                                                    options={gradientDashboardPanelChart}
                                                />
                                            </div>
                                            <Table responsive>
                                                {/*<tbody>{this.createTableData()}</tbody>*/}
                                            </Table>
                                        </CardBody>
                                    </Card>
                                </Col>
                            </Row>

                        </Col>
                    </Row>
                </div>
            </div>
        )
    }

}

export default index;
