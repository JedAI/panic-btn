import windowReducer, {loginReducer} from './sec/reducer'

const initState = {};
export default (state = initState, action) => {
    return {
        ...state,
        user: loginReducer(state.user, action),
        auth: windowReducer(state.auth, action),
    }
}