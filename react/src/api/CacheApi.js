import axios from "axios";
import {cfg} from "../util/General";
import moment from "moment";

export const find_usuario = (id) => {
    let key = 'clt_' + id;
    return new Promise(function (resolve, reject) {
        let value = from_cache_window(key);
        if (value == null) {
            axios.get(cfg.api + '/usuario', {params: {id: id}}).then(resp => {
                let item = resp.data;
                to_cache_window(key, item);
                resolve(item);
            }).catch(error => {
                    this.alert.current.handle_error(error);
                }
            );
        } else {
            resolve(value);
        }
    });
};

export const from_cache_window = (key, minutes = 120) => {
    let storage = window.storage || {};
    window.storage = storage;
    let item = storage[key];

    if (item == null) {//no esta en storage ventana
        item = from_cache(key);
        if (item == null) { // no esta en local storage
            return null;
        } else { // si esta en local storage pero no en window
            to_cache_window(key, item); // pongo en window
            return item;
        }
    } else {
        try {
            let now = moment(new Date()); //todays date
            let end = moment(item.fc); // another date
            let duration = moment.duration(now.diff(end));
            let diff = duration.asMinutes();
            if (diff >= minutes) {
                return null;
            }
            return item.value;
        } catch (e) {
            return null;
        }
    }
};

export const from_cache = (key, minutes = 120) => {
    let item = sessionStorage.getItem(key);
    minutes = minutes || 15;

    if (item == null) {
        return null;
    } else {
        try {
            item = JSON.parse(item);
            let now = moment(new Date()); //todays date
            let end = moment(item.fc); // another date
            let duration = moment.duration(now.diff(end));
            let diff = duration.asMinutes();
            if (diff >= minutes) {
                return null;
            }
            return item.value;
        } catch (e) {
            return null;
        }
    }
};

export const to_cache = (key, val) => {
    sessionStorage.setItem(key, JSON.stringify({value: val, fc: new Date()}));
};

export const to_cache_window = (key, val) => {
    let storage = window.storage || {};
    window.storage = storage;

    storage[key] = {value: val, fc: new Date()};

    to_cache(key, val);
};

export const clear_all_cache = () => {
    window.storage = null;
    sessionStorage.clear();
};