import React, {Component} from 'react';
import logo from '../logo.svg';

class About extends Component {

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <p>
                        <div className="copyright" id="copyright">
                            &copy;
                            Derecho Exclusivos de
                            <a href="#"> Coro A & Imbaquingo J</a>
                        </div>
                    </p>
                </header>
            </div>
        );
    }
}

export default About;