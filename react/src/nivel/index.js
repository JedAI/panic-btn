import React, {Component} from 'react';
import axios from "axios";
import {cfg} from "../util/General";
import Alerts from "../util/error";
import {
    Card,
    CardBody,
    CardHeader,
    CardTitle,
    Row,
    Col,
    Table,
    Modal,
    ModalFooter,
    ModalBody,
    ModalHeader,
    Button,
    Label
} from "reactstrap";
import PanelHeader from "../components/PanelHeader/PanelHeader";
import Datetime from "react-datetime";
import moment from "moment";
import Chart from "../coms/BlkC3";
import ReactTable from "react-table";
import {TipoUsuario, NombreUsuario, FacuUsuario} from "../coms/BlkGenerales"
import {Line} from "react-chartjs-2";
import {Statistics, CardCategory} from "../components";
import {BlkField} from "../coms/BlkField";
import BlkFormik from "../coms/BlkFormik";
import {Form} from "formik";

const gradientDashboardPanelChart = {
    layout: {
        padding: {
            left: 20,
            right: 20,
            top: 0,
            bottom: 0
        }
    },
    maintainAspectRatio: false,
    tooltips: {
        backgroundColor: "#fff",
        titleFontColor: "#333",
        bodyFontColor: "#666",
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
    },
    legend: {
        position: "bottom",
        fillStyle: "#FFF",
        display: false
    },
    scales: {
        yAxes: [
            {
                ticks: {
                    fontColor: "rgba(255,255,255,0.4)",
                    fontStyle: "bold",
                    beginAtZero: true,
                    maxTicksLimit: 5,
                    padding: 10
                },
                gridLines: {
                    drawTicks: true,
                    drawBorder: false,
                    display: true,
                    color: "rgba(255,255,255,0.1)",
                    zeroLineColor: "transparent"
                }
            }
        ],
        xAxes: [
            {
                gridLines: {
                    display: false,
                    color: "rgba(255,255,255,0.1)"
                },
                ticks: {
                    padding: 10,
                    fontColor: "rgba(255,255,255,0.4)",
                    fontStyle: "bold"
                }
            }
        ]
    }
}

class index extends Component {

    constructor() {
        super(...arguments);
        this.alert = React.createRef();
        this.state = {
            datos: {
                alertas: [],
                niveles: [],
                motivo: [],
                atencion: [],
                recibidas: 0
            },
            datos1: {
                alertas: [],
                alertasMes: 0,
                recibidas: {}
            },
            q: {
                fi: new Date(),
                ff: new Date()
            },
            list: [],
            item: {},
            modalOpen: false
        }
    }

    componentDidMount() {
        this.load();
        this.loadDatos();
    }

    onChangeValueI = (event) => {
        let q = Object.assign({}, this.state.q);
        let doQuery = true;
        if (event instanceof moment) {
            q.fi = event;
            if (!q.fi.isSameOrBefore(q.ff, "day")) {
                this.alert.current.show_info('La fecha final deber ser mayor a al fecha inicial');
                doQuery = false;
            }
        }
        if (doQuery && q.fi && q.ff) {
            this.setState({q}, () => {
                this.loadDatos();
            });
        } else {
            this.setState({q, datos: {}});
        }
    };

    onChangeValueF = (event) => {
        let q = Object.assign({}, this.state.q);
        let doQuery = true;
        if (event instanceof moment) {
            q.ff = event;
            if (!q.ff.isSameOrAfter(q.fi, "day")) {
                this.alert.current.show_info('La fecha final deber ser mayor a al fecha inicial');
                doQuery = false;
            }
        }

        if (doQuery && q.fi && q.ff) {
            this.setState({q}, () => {
                this.loadDatos();
            });
        } else {
            this.setState({q, datos: {}});
        }
    };

    load = () => {
        axios.get(cfg.api + '/resumen').then(resp => {
            this.setState({datos1: resp.data});
        }).catch(error => {
            this.alert.current.handle_error(error);
        });
    }

    loadDatos = () => {
        let body = {
            fi: this.state.q.fi,
            ff: this.state.q.ff
        };
        axios.post(cfg.api + "/nivelservicio", body).then(resp => {
            let datos = resp.data;
            let niveles = datos.niveles;
            let alertas = datos.alertas;
            let list = [];
            let grafico1 = this.builDonut(datos.estado, "Estado")
            let grafico2 = this.builDonut(datos.motivo, "Motivo")
            let grafico3 = this.builDonut(datos.genero, "Genero")
            let grafico4 = this.builDonut(datos.facu, "Facultad")
            if (niveles.length > 0) {
                list = niveles.map(n => {
                    let item = Object.assign({}, n);//clono el item
                    item.idU = alertas.find(a => a.id === item.idA).idUsuario;
                    item.idG = alertas.find(a => a.id === item.idA).idGuardia;
                    item.fc = alertas.find(a => a.id === item.idA).fcAlerta;
                    return item;
                });
            }
            this.setState({datos, grafico1, grafico2, grafico3, grafico4, list});
        }).catch(error => {
            this.alert.current.handle_error(error);
        });
    }

    builDonut = (datos, titulo) => {
        let donutChart = {
            data: {
                columns: datos,
                type: 'donut',
            },
            donut: {
                title: titulo
            }
        };
        return donutChart;
    };

    onSubmit = (newValues, actions) => {
        axios.post(cfg.api + '/nivelservicio', newValues, {
            params: {
                id: 1
            }
        }).then(() => {
            this.loadDatos();
            this.alert.current.show_info('Guardado con éxito');
            actions.setSubmitting(false);
            this.setState({modalOpen: false});
        }).catch(error => {
                this.setState({modalOpen: false});
                this.alert.current.handle_error(error);
                actions.setSubmitting(false);
            }
        );
    };

    closedModal = () => {
        this.setState({modalOpen: false});
    };

    render() {

        const dataPanelChart = (canvas) => {
            const ctx = canvas.getContext("2d");
            var chartColor = "#FFFFFF";
            var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
            gradientStroke.addColorStop(0, "#80b6f4");
            gradientStroke.addColorStop(1, chartColor);
            var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
            gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
            gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.14)");

            return {
                labels: [
                    "ENE",
                    "FEB",
                    "MAR",
                    "ABR",
                    "MAY",
                    "JUN",
                    "JUL",
                    "AGO",
                    "SEP",
                    "OCT",
                    "NOV",
                    "DIC"
                ],
                datasets: [
                    {
                        label: "Alertas",
                        borderColor: chartColor,
                        pointBorderColor: "#FFF",
                        pointBackgroundColor: "#f96332",
                        pointHoverBorderColor: chartColor,
                        pointBorderWidth: 1,
                        pointHoverRadius: 7,
                        pointHoverBorderWidth: 2,
                        pointRadius: 5,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: this.state.datos1.alertas
                    }
                ]
            };
        }

        return (
            <div>
                <Alerts ref={this.alert}/>
                <PanelHeader size="sm"/>
                <div className="content">
                    <Row>
                        <Col>
                            <div className="header-panic-dashboard-tittle">
                                <b>ALERTAS EMITIDAS</b>
                            </div>
                            <div className="header-panic-dashboard">
                                <Line
                                    data={dataPanelChart}
                                    options={gradientDashboardPanelChart}
                                />
                            </div>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col xs={12} md={12}>
                            <Card className="card-stats card-raised">
                                <CardBody>
                                    <Row>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="success"
                                                icon="ui-2_chat-round"
                                                title={this.state.datos1.recibidas.alertas}
                                                subtitle="Alertas Recibidas"
                                            />
                                        </Col>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="primary"
                                                icon="ui-2_chat-round"
                                                title={this.state.datos1.alertasMes}
                                                subtitle="Alertas Mes"
                                            />
                                        </Col>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="info"
                                                icon="users_single-02"
                                                title={this.state.datos1.recibidas.registrados}
                                                subtitle="Usuarios Registrados"
                                            />
                                        </Col>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="succes"
                                                // icon="objects_support-17"
                                                icon="sport_user-run"
                                                title={this.state.datos1.recibidas.promedio + " min"}
                                                subtitle="Tiempo de Respuesta"
                                            />
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={12}>
                            <Card>
                                <CardHeader>
                                    <CardTitle>
                                        ESTADISTICAS
                                    </CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <div className="form-group col-6 row">
                                            <label htmlFor="fi" className="col-md-4">Ingrese la fecha inicio:</label>
                                            <Datetime name="fi" value={this.state.q.fi}
                                                      className="col-md-8 px-0"
                                                      dateFormat={'DD/MM/YYYY'}
                                                      timeFormat={false}
                                                      onChange={this.onChangeValueI}
                                                      closeOnSelect={true}
                                            />
                                        </div>
                                        <div className="form-group col-6 row">
                                            <label htmlFor="ff" className="col-md-4">Ingrese la fecha fin:</label>
                                            <Datetime name="ff" value={this.state.q.ff}
                                                      className="col-md-8 px-0"
                                                      dateFormat={'DD/MM/YYYY'}
                                                      timeFormat={false}
                                                      onChange={this.onChangeValueF}
                                                      closeOnSelect={true}
                                            />
                                        </div>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={6} md={6}>
                            <Card>
                                <CardHeader>
                                    <CardTitle>
                                        ESTADO
                                    </CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <div className="col-12">
                                            {this.state.grafico1 && this.state.list.length > 0 &&
                                            <Chart {...this.state.grafico1}/>}
                                        </div>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs={6} md={6}>
                            <Card>
                                <CardHeader>
                                    <CardTitle>
                                        MOTIVOS
                                    </CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <div className="col-12">
                                            {this.state.grafico2 && this.state.list.length > 0 &&
                                            <Chart {...this.state.grafico2}/>}
                                        </div>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={6} md={6}>
                            <Card>
                                <CardHeader>
                                    <CardTitle>
                                        GENERO
                                    </CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <div className="col-12">
                                            {this.state.grafico3 && this.state.list.length > 0 &&
                                            <Chart {...this.state.grafico3}/>}
                                        </div>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs={6} md={6}>
                            <Card>
                                <CardHeader>
                                    <CardTitle>
                                        FACULTAD
                                    </CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <div className="col-12">
                                            {this.state.grafico4 && this.state.list.length > 0 &&
                                            <Chart {...this.state.grafico4}/>}
                                        </div>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Card>
                                <CardHeader>
                                    <CardTitle>
                                        DETALLE {this.state.list.length > 0 &&
                                    <span> / <b> {this.state.list.length}</b> ALERTAS RECIBIDAS</span>}
                                    </CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        {this.state.list.length > 0 &&
                                        <div className="col-12">
                                            <ReactTable
                                                noDataText={"No hay datos..."}
                                                loadingText={"Cargando..."}
                                                rowsText={"filas"}
                                                ofText={"de"}
                                                previousText={"Anterior"}
                                                nextText={"Siguiente"}
                                                pageText={"Página"}
                                                columns={[
                                                    {
                                                        Header: "N.",
                                                        accessor: 'id',
                                                        Cell: row => {
                                                            return <div>{row.index + 1}</div>;
                                                        },
                                                        width: 40,
                                                        hideFilter: true,
                                                        filterable: false,
                                                        minResizeWidth: 10
                                                    },
                                                    {
                                                        id: "fi",
                                                        Header: "Fecha / Hora",
                                                        filterable: false,
                                                        width: 200,
                                                        accessor: d => {
                                                            return moment(d.fc).local().format("DD-MM-YYYY / HH:mm")
                                                        },
                                                        minResizeWidth: 10
                                                    },
                                                    {
                                                        Header: "Facultad",
                                                        accessor: "idU",
                                                        filterable: false,
                                                        width: 300,
                                                        Cell: row => <FacuUsuario cod={row.original.idU}
                                                                                  className="check-center"/>,
                                                        minResizeWidth: 10
                                                    },
                                                    {
                                                        Header: "Tipo",
                                                        accessor: "idU",
                                                        filterable: false,
                                                        width: 200,
                                                        Cell: row => <TipoUsuario cod={row.original.idU}
                                                                                  className="check-center"/>,
                                                        minResizeWidth: 10
                                                    },
                                                    {
                                                        Header: "Usuario",
                                                        accessor: "idU",
                                                        filterable: false,
                                                        width: 250,
                                                        Cell: row => <NombreUsuario cod={row.original.idU}
                                                                                    className="check-center"/>,
                                                        minResizeWidth: 10
                                                    },
                                                    {
                                                        Header: "Guardia",
                                                        accessor: "idG",
                                                        filterable: false,
                                                        width: 250,
                                                        Cell: row => <NombreUsuario cod={row.original.idG}
                                                                                    className="check-center"/>,
                                                        minResizeWidth: 10
                                                    },
                                                    {
                                                        Header: "Motivo",
                                                        accessor: "m",
                                                        width: 150,
                                                        minResizeWidth: 10
                                                    },
                                                    {
                                                        Header: "Estado",
                                                        accessor: "na",
                                                        width: 250,
                                                        minResizeWidth: 10
                                                    },
                                                    {
                                                        Header: "Observación",
                                                        accessor: "obs",
                                                        filterable: false,
                                                        width: 450,
                                                        minResizeWidth: 10
                                                    },
                                                    {
                                                        Header: " ",
                                                        filterable: false,
                                                        Cell: row => <div>
                                                            <span className="text-info option px-2"
                                                                  title="Ingresar Observación" onClick={() => {
                                                                this.setState({
                                                                    modalOpen: true,
                                                                    item: row.original
                                                                })
                                                            }}>
                                                        <i className="fas fa-edit mx-2"/>
                                                                       </span>
                                                        </div>
                                                    }
                                                ]}
                                                data={this.state.list}
                                                getTdProps={(state, rowInfo) => {
                                                    if (rowInfo && rowInfo.row) {
                                                        return {
                                                            style: {
                                                                cursor: "pointer",
                                                            }
                                                        }
                                                    } else {
                                                        return {}
                                                    }
                                                }}
                                                filterable
                                                defaultPageSize={10}
                                                showPaginationBottom
                                                className="-striped -highlight">
                                            </ReactTable>
                                        </div>}
                                    </Row>
                                </CardBody>
                                <Modal className="text-center" isOpen={this.state.modalOpen}>
                                    <BlkFormik initialValues={this.state.item} onSubmit={this.onSubmit}
                                               enableReinitialize={true} validateOnChange={false}>
                                        {({values, isSubmitting, errors, handleChange, resetForm, submitForm}) => (
                                            <Form>
                                                <ModalHeader className="justify-content-center title" tag="h4">
                                                    Ingresar Observación
                                                </ModalHeader>
                                                <ModalBody>
                                                    <Row>
                                                        <div className="form-group col-lg-12">
                                                            <Label>Observación: </Label>
                                                            <BlkField name="obs" component="textarea"
                                                                      className="form-control"/>
                                                        </div>
                                                    </Row>
                                                </ModalBody>
                                                <ModalFooter>
                                                    <ModalFooter>
                                                        <Button color="primary" disabled={isSubmitting}
                                                                className={"mx-2"} onClick={submitForm}>
                                                            Guardar
                                                        </Button>
                                                        <button type={"button"} className="btn btn-default"
                                                                onClick={this.closedModal}>
                                                            Cancelar
                                                        </button>
                                                    </ModalFooter>
                                                </ModalFooter>
                                            </Form>
                                        )}
                                    </BlkFormik>
                                </Modal>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default index;