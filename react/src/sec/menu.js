import React, {Component} from "react";
import Alerts from "../util/error";
import {Nav} from "reactstrap";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import logo from '../logo.svg';
import PerfectScrollbar from "perfect-scrollbar";
import Button from "../components/CustomButton/CustomButton";

var ps;
const LinkMenu = (props) => {
    if (props.k) {
        return (
            <Link to={props.k}
                  onClick={props.onClick}>
                <i className={"now-ui-icons " + props.icon}/>
                <p>
                    {props.titulo}
                </p>
            </Link>
        );
    } else {
        return (
            <a data-toggle="collapse" onClick={props.onClick}>
                <i className={"now-ui-icons " + props.icon}/>
                <p>
                    {props.titulo}
                    <b className="caret"/>
                </p>
            </a>
        );
    }
};

class Menu extends Component {
    constructor(props) {
        super(props);
        this.alert = React.createRef();
        this.state = {
            openAvatar: false
        };

        this.state = {
            menu: []
        };
        this.minimizeSidebar = this.minimizeSidebar.bind(this);
    }

    minimizeSidebar() {
        var message = "Sidebar mini ";
        if (document.body.classList.contains("sidebar-mini")) {
            message += "deactivated...";
        } else {
            message += "activated...";
        }
        document.body.classList.toggle("sidebar-mini");
        var options = {};
        options = {
            place: "tr",
            message: message,
            type: "info",
            icon: "now-ui-icons ui-1_bell-53",
            autoDismiss: 7
        };
    }

    componentDidMount() {
        if (navigator.platform.indexOf("Win") > -1) {
            ps = new PerfectScrollbar(this.refs.sidebar, {
                suppressScrollX: true,
                suppressScrollY: false
            });
        }
    }

    componentWillUnmount() {
        if (navigator.platform.indexOf("Win") > -1) {
            ps.destroy();
        }
        // to stop the warning of calling setState of unmounted component
        var id = window.setTimeout(null, 0);
        while (id--) {
            window.clearTimeout(id);
        }
    }

    render() {
        return (
            <div>
                <div>
                    <Alerts ref={this.alert}/>
                    <div className="sidebar">
                        <div className="logo">
                            <a
                                href="https://www.creative-tim.com"
                                className="simple-text logo-mini"
                            >
                                <div className="logo-img">
                                    <img src={logo} alt="react-logo"/>
                                </div>
                            </a>
                            <a href="/" className="simple-text logo-normal" style={{textTransform: 'none'}}>
                                SmartAlert-UCE
                            </a>
                            <div className="navbar-minimize">
                                <Button
                                    simple
                                    neutral
                                    icon
                                    round
                                    id="minimizeSidebar"
                                    onClick={this.minimizeSidebar}>
                                    <i className="now-ui-icons text_align-center visible-on-sidebar-regular"/>
                                    <i className="now-ui-icons design_bullet-list-67 visible-on-sidebar-mini"/>
                                </Button>
                            </div>
                        </div>

                        <div className="sidebar-wrapper" ref="sidebar">
                            <Nav>
                                <li>
                                    <Link to={"/realtime"}>
                                        <i className="now-ui-icons objects_globe"/>
                                        <p className="sidebar-normal">Real Time</p>
                                    </Link>
                                </li>
                                <br/>
                                <li>
                                    <Link to={"/estadisticas"}>
                                        <i className="now-ui-icons ui-1_calendar-60"/>
                                        <p className="sidebar-normal">Estadisticas</p>
                                    </Link>
                                </li>
                            </Nav>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    user: state.user
});

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Menu);