import React, {Component} from 'react';
import axios from 'axios';
import {do_login} from './actions'
import {connect} from "react-redux";
import logo from '../logo.svg';
import {
    Card,
    CardBody,
    CardFooter,
    CardTitle,
    Col,
    Container,
    Form,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText
} from "reactstrap";
import Button from "../components/CustomButton/CustomButton";
import bgImage from "../assets/img/bg3.jpg";
import Alerts from "../util/error";
import {cfg} from "../util/General";
import Footer from "../components/Footer/Footer";

class Login extends Component {

    constructor(props) {
        super(props);
        this.alert = React.createRef();
    };

    /**
     * se ejecuta cuando el usuario da en inicar sesion
     *
     * @param event
     */
    onRequestAuth(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        //headers q lee el spring security (efi-sec)
        let headers = {authorization: 'Basic ' + btoa(data.get('enki') + ':' + data.get('perseo'))};

        // envio de peticion
        axios.defaults.withCredentials = true;
        axios.get(cfg.apiSec + '/enki', {headers: headers}).then(resp => {
            // hay respuesta exitosaa desde el server, si se inicio sesión
            //lanzo accion con los datos del usuario
            console.log(this.props);
            this.props.history.replace('/');
            this.props.do_login(resp.data);

        }).catch(error => {
                this.alert.current.handle_error(error);
                this.props.do_login({username: null, id: null});
            }
        );
    }

    render() {
        return (
            <div className="wrapper wrapper-full-page ">
                <Alerts ref={this.alert}/>
                <div className="full-page login-page section-image" filter-color="black">
                    <div className="content">
                        <Container>
                            <Col md={4} className="ml-auto mr-auto">
                                <Form onSubmit={this.onRequestAuth.bind(this)}>
                                    <Card className={"card card-login card-plain"}>
                                        <div className="text-center">
                                            <img src={logo} className="App-logo" alt="logo"/>
                                            <CardTitle>Ingrese su Usuario y Contraseña</CardTitle>
                                        </div>
                                        <CardBody>
                                            <InputGroup className={"input-group no-border form-control-lg"}>
                                                <InputGroupAddon className={"input-group-prepend"} addonType="prepend">
                                                    <InputGroupText className={"input-group-text"}>
                                                        <i className="now-ui-icons users_circle-08"></i>
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input className="form-control" type="text" placeholder="Usuario"
                                                       name="enki"/>
                                            </InputGroup>
                                            <InputGroup className="no-border form-control-lg ">
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="now-ui-icons text_caps-small"></i>
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input className="form-control" type="password" placeholder="Contraseña"
                                                       name="perseo"/>
                                            </InputGroup>
                                        </CardBody>
                                        <CardFooter>
                                            <Button block round color="primary" size="lg" className="mb-3">
                                                Ingresar
                                            </Button>
                                        </CardFooter>
                                    </Card>
                                </Form>
                            </Col>
                        </Container>
                    </div>
                    <Footer/>
                    <div className="full-page-background"
                         style={{backgroundImage: "url(" + bgImage + ")"}}/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    user: state.user
});

const mapDispatchToProps = {
    do_login
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);