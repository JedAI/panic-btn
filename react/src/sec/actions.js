const SEC_A = {
    DO_RESET: 'DO_RESET',
    ON_AUTH: 'ON_AUTH',
    ON_LOGIN: 'ON_LOGIN',
    REQUEST_EDIT: 'REQUEST_EDIT',
    ON_EDIT: 'ON_EDIT'
};
export default SEC_A;

export const do_reset = () => {

};

export const do_auth = content => {
    return {
        type: SEC_A.ON_AUTH,
        payload: content
    }
};

export const do_login = content => {
    return {
        type: SEC_A.ON_LOGIN,
        payload: content
    }
};

export const request_edit = (btn) => {
    return {
        type: SEC_A.REQUEST_EDIT,
        payload: {btn: btn}
    }
};

export const do_edit = (content) => {
    return {
        type: SEC_A.ON_EDIT,
        payload: content
    }
};