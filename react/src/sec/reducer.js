import SEC_A from './actions'

const windowReducer = (state = {auth: false, requestEdit: false}, action) => {
    switch (action.type) {
        case SEC_A.DO_RESET:
            return {auth: false, edit: true};
        case SEC_A.ON_AUTH:
            return action.payload;
        case SEC_A.REQUEST_EDIT:
            return {...state, requestEdit: true, btn: action.payload.btn};
        case SEC_A.ON_EDIT:
            return {...state, requestEdit: false, ...action.payload};
        default:
            return state;
    }
};

export default windowReducer;

export const loginReducer = (state = {username: null, id: null, tipo: null}, action) => {
    switch (action.type) {
        case SEC_A.ON_LOGIN:
            return action.payload;
        default:
            return state;
    }
};