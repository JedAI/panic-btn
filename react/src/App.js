import React, {Component} from 'react';
import {Route, withRouter} from 'react-router-dom';
import {connect} from "react-redux";
import './App.css';
import About from "./home/About";
import Realtime from "./realtime/list";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Menu from "./sec/menu";
import Login from "./sec/login";
import Dashboard from "./dashboard/index";
import Consultas from "./nivel/index";

class App extends Component {
    render() {
        return (
            this.props.user.id ?
                <div className="justify-content-center">
                    <div className="main-panel" ref="mainPanel">
                        <Header/>
                        <Menu/>
                        <main>
                            <Route exact path="/" component={Realtime}/>
                            <Route path="/home" component={About}/>
                            <Route path="/realtime" component={Realtime}/>
                            <Route path="/estadisticas" component={Consultas}/>
                        </main>
                        <Footer/>
                    </div>
                </div>
                :
                <Route path="/" component={Login}/>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    user: state.user
});

const mapDispatchToProps = {};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(App));
