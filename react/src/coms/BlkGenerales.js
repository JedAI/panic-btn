import React, {Component} from 'react';
import moment from "moment";
import {find_usuario} from "../api/CacheApi";

export class NombreUsuario extends Component {

    constructor() {
        super(...arguments);
        this.state = {
            nm: null
        }
    }

    fetchData = () => {
        if (this.props.cod) {
            find_usuario(this.props.cod).then(clt => this.setState({nm: clt.nombre}));
        }
    };

    componentDidMount() {
        this.fetchData();
    }

    componentDidUpdate(prevProps) {
        if (this.props.cod !== prevProps.cod) {
            this.fetchData();
        }
    }

    render() {
        return (<span>
            {this.state.nm}
        </span>)
    }

}

export class TipoUsuario extends Component {

    constructor() {
        super(...arguments);
        this.state = {
            nm: null
        }
    }

    fetchData = () => {
        if (this.props.cod) {
            find_usuario(this.props.cod).then(clt => this.setState({nm: clt.tipo}));
        }
    };

    componentDidMount() {
        this.fetchData();
    }

    componentDidUpdate(prevProps) {
        if (this.props.cod !== prevProps.cod) {
            this.fetchData();
        }
    }

    render() {
        return (<span>
            {this.state.nm}
        </span>)
    }

}

export class FacuUsuario extends Component {

    constructor() {
        super(...arguments);
        this.state = {
            nm: null
        }
    }

    fetchData = () => {
        if (this.props.cod) {
            find_usuario(this.props.cod).then(clt => this.setState({nm: clt.idFacultad}));
        }
    };

    componentDidMount() {
        this.fetchData();
    }

    componentDidUpdate(prevProps) {
        if (this.props.cod !== prevProps.cod) {
            this.fetchData();
        }
    }

    render() {
        return (<span>
            {this.state.nm}
        </span>)
    }

}