import React, {Component} from 'react';
import c3 from 'c3';

class Chart extends Component {

    constructor(props) {
        super(props);

        this.target = React.createRef();
    }

    componentDidMount() {
        // When the component mounts the first time we update the chart.
        this.updateChart(this.props);

    }

    componentWillReceiveProps(nextProps, nextContext) {
        // When we receive a new prop then we update the chart again.
        this.updateChart(nextProps);

        if (nextProps.onPropsChanged) {
            nextProps.onPropsChanged(this.props, nextProps, this.chart);
        }
    }

    updateChart = (config) => {
        if (!this.chart) {
            this.chart = this.generateChart(this.target.current, config);
        }

        this.chart.load(config.data);
    };

    generateChart = (mountNode, config) => {
        const newConfig = Object.assign({bindto: mountNode}, config);
        return c3.generate(newConfig);
    };

    componentWillUnmount() {
        try {
            this.chart = this.chart.destroy();
        } catch (err) {
            throw new Error(err);
        }
    }

    render() {
        return <div ref={this.target}/>;
    }
}

export default Chart;