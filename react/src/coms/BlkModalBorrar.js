import {Modal, ModalBody, ModalFooter, ModalHeader,} from "reactstrap";
import React from "react";

export const BlkModalBorrar = ({isOpen, toggle, data, onClosed}) => {

    return (
        <Modal className="text-center" isOpen={isOpen} toggle={toggle}>
            <ModalHeader className="justify-content-center title" tag="h4">
                Borrar!
            </ModalHeader>
            <ModalBody>
                <p>
                    ¿Desea dar por finalizada la alerta?
                </p>
            </ModalBody>
            <ModalFooter>
                <button
                    className="btn btn-primary"
                    onClick={() => {
                        onClosed(data);
                        toggle();
                    }}>
                    Si
                </button>
                <button className="btn btn-danger"
                        onClick={toggle}>
                    No
                </button>
            </ModalFooter>
        </Modal>
    );
};