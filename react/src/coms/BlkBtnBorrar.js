import React, {Component} from "react";
import {BlkModalBorrar} from "./BlkModalBorrar";

export class BlkBtnBorrar extends Component {
    constructor() {
        super(...arguments);
        this.state = {
            isOpen: false,
        }
    };

    toggleBorrarDialog() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    };

    render() {
        return (
            <div>
                <a className="form-control btn btn-primary"
                   onClick={() => {
                       this.setState({isOpen: true})
                   }}>
                    <i className="fa fa-trash"></i>
                </a>

                <BlkModalBorrar
                    isOpen={this.state.isOpen}
                    toggle={this.toggleBorrarDialog.bind(this)}
                    data={this.props.data}
                    onClosed={this.props.onClosed}/>
            </div>
        );
    }

};