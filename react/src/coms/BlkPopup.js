import React from "react";
import {Popup} from 'react-leaflet';

export const BlkPopup = ({marker, ...props}) => (
    <Popup className="leaflet-popup" id={marker.id}>
        <b>{marker.tipo}</b><br/>
        <span>Nombre: <b>{marker.nm}</b></span>
        <div className="user">
        </div>
    </Popup>
);