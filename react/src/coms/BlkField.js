import {ErrorMessage, Field} from "formik";
import React from "react";

export const BlkField = (props) => {
    return (
        <React.Fragment>
            <Field {...props}/>
            <ErrorMessage name={props.name}>
                {(error) => (
                    <span className={'invalid-feedback d-block'}>{error}</span>
                )}
            </ErrorMessage>
        </React.Fragment>
    );
};