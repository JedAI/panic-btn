import React, {Component} from 'react';
import 'axios-progress-bar/dist/nprogress.css'
import {loadProgressBar} from "axios-progress-bar";

const s_none = {display: "none"};
export default class BlkLoadingBar extends Component {

    componentDidMount() {
        loadProgressBar();
    }

    componentWillUnmount() {
    }

    // método que será llamado cada x segundos

    render() {
        return (
            <span style={s_none}>loading...</span>
        );
    }
}