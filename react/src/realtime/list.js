import React, {Component} from 'react';
import axios from "axios";
import {cfg} from "../util/General";
import update from 'react-addons-update';
import {Map, Marker, TileLayer} from 'react-leaflet';
import Leaflet from "leaflet";
import Alerts from "../util/error";
import {Card, CardBody, CardHeader, CardTitle, Table} from "reactstrap";
import map from "lodash/map";
import RealtimeSocket from "./RealtimeSocket";
import {BlkPopup} from "../coms/BlkPopup";
import {BlkBtnBorrar} from "../coms/BlkBtnBorrar";

const cover = {position: 'absolute', left: 0, right: 0, top: 60, bottom: 60};

const iconN = new Leaflet.Icon({
    iconUrl: require('../assets/img/policeman.png'),
    // shadowUrl: require('../assets/img/marker-icon.png'),
    iconSize: [15, 29], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor: null, // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor: [0, 0]// point from which the popup should open relative to the iconAnchor
})

const iconA = new Leaflet.Icon({
    iconUrl: require('../assets/img/man-with-a-flag.png'),
    // shadowUrl: require('../assets/img/marker-icon.png'),
    iconSize: [15, 29], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor: null, // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor: [0, 0]// point from which the popup should open relative to the iconAnchor
})

class Realtime extends Component {

    constructor() {
        super(...arguments);
        this.alert = React.createRef();
        this.state = {
            showMenu: false,
            ids: [],
            markers: [],
            position: [-0.198491, -78.502958]
        };
        this.showMenu = this.showMenu.bind(this);
    };

    showMenu(event) {
        event.preventDefault();
        this.setState({showMenu: !this.state.showMenu});
    }

    updateMarkers(data) {
        console.log(data);
        const index = this.state.markers.findIndex((emp) => emp.id === data.id);

        let markers;
        let ids = this.state.ids;
        if (index >= 0) {
            markers = update(this.state.markers, {$splice: [[index, 1, data]]});  // array.splice(start, deleteCount, item1)
            if (data.tipo !== 'GUARDIA') {
                const index2 = this.state.ids.findIndex((emp) => emp.id === data.id);
                ids = update(this.state.ids, {$splice: [[index2, 1, data]]});  // array.splice(start, deleteCount, item1)
                this.setState({ids: ids});
            }
        } else {
            markers = update(this.state.markers, {$push: [data]});
            if (data.tipo !== 'GUARDIA') {
                this.alert.current.show_info("Alerta emitida por: " + data.nm);
                ids = update(this.state.ids, {$push: [data]});
                this.setState({ids: ids, showMenu: true});
            }
        }

        this.setState({markers: markers});
        console.log(ids);
        console.log(markers);
    };

    onClosed = (data) => {

        //llamar metodo de closed
        const index = this.state.markers.findIndex((emp) => emp.id === data.id);

        let markers;
        let ids;

        markers = update(this.state.markers, {$splice: [[index, 1]]});  // array.splice(start, deleteCount, item1)
        ids = update(this.state.ids, {$splice: [[index, 1]]});  // array.splice(start, deleteCount, item1)

        this.setState({
            markers: markers
        });

        this.setState({
            ids: ids
        });
    };

    render() {

        return (
            <div className="App">
                <Alerts ref={this.alert}/>
                <div className="App-header">
                    <RealtimeSocket subsribeUrl={cfg.apiSec + '/ws'}
                                    updateMarkers={this.updateMarkers.bind(this)} alert={this.alert.current}>
                        <Map center={this.state.position} zoom={18} style={cover}>
                            <TileLayer
                                url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'/>
                            {
                                map(this.state.markers, (marker) => {
                                    const {tipo} = marker;
                                    let icon = null;
                                    if (tipo === 'GUARDIA') {
                                        icon = iconN;
                                    } else {
                                        icon = iconA;
                                    }
                                    return (
                                        <Marker key={marker.id} icon={icon} title={marker.nm} className="xxx"
                                                alt={marker.id} position={[marker.latitude, marker.longitude]}>
                                            <BlkPopup marker={marker}/>
                                        </Marker>
                                    )
                                })
                            }
                        </Map>
                    </RealtimeSocket>
                </div>
                <div className="fixed-plugin">
                    <div className="dropdown show">
                        <a onClick={this.showMenu} className="text-primary">
                            <span>
                                <i className="now-ui-icons text_align-center visible-on-sidebar-regular"/>
                            </span>
                        </a>
                        {this.state.showMenu ?
                            (<div className="dropdown-menu show">
                                    <Card>
                                        <CardHeader>
                                            <CardTitle className="text-primary">
                                                Alertas Producidas
                                            </CardTitle>
                                        </CardHeader>
                                        <CardBody>
                                            <dic className="table-responsive resposi-panic">
                                                <Table
                                                    className="table table-sticky">
                                                    <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Usuario</th>
                                                        <th>Guardia</th>
                                                        <th>Accion</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {map(this.state.ids, (data, index) => (
                                                        <tr key={data.id}>
                                                            <td>
                                                                {index + 1}
                                                            </td>
                                                            <td onClick={() => {
                                                                document.querySelectorAll('[alt="' + data.id + '"]')[0].click();

                                                            }}>
                                                                {data.nm}
                                                            </td>
                                                            <td onClick={() => {
                                                                if (data.idG != "Sin Asignar") {
                                                                    document.querySelectorAll('[alt="' + data.idG + '"]')[0].click();
                                                                }
                                                            }}>
                                                                {data.nmG}
                                                            </td>
                                                            <td>
                                                                <BlkBtnBorrar
                                                                    data={data}
                                                                    onClosed={this.onClosed}/>
                                                            </td>
                                                        </tr>
                                                    ))}
                                                    </tbody>
                                                </Table>
                                            </dic>
                                        </CardBody>
                                    </Card>
                                </div>
                            ) : (
                                null
                            )}
                    </div>
                </div>
            </div>

        );
    }
}

export default Realtime;