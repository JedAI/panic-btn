import React, {Component} from "react";
import axios from "axios";
import {cfg} from "../util/General";
//https://github.com/browniefed/pdxlivebus/blob/master/app/containers/VehicleSocket.js
var Stomp = require('stomp-websocket');
var SockJS = require('sockjs-client');


class RealtimeSocket extends Component {

    constructor() {
        super();
        this.socket = null;
    }

    componentDidMount() {
        console.log('server -->' + this.props.subsribeUrl);
        this.socket = new SockJS(this.props.subsribeUrl);
        this.stompClient = Stomp.over(this.socket);
        this.stompClient.connect({}, (frame) => {
            console.log('Connected: ' + frame);
            this.stompClient.subscribe('/topic/point_update', (greeting) => {
                this.props.updateMarkers(JSON.parse(greeting.body));
            });
        });

        axios.get(cfg.api + '/notificacion').then(() => {
        }).catch(error => {
            this.props.alert.handle_error(error);
        })
    }

    componentWillUnmount() {
        //disconnect scoket
        this.stompClient.disconnect({}, (frame) => {
            console.log('Disconnected: ' + frame);
        });
    }

    render() {
        return (
            this.props.children
        )
    }
}

export default RealtimeSocket;
