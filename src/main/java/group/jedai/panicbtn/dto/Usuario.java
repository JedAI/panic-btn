package group.jedai.panicbtn.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.tomcat.jni.Local;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Usuario implements Serializable {

    @Id
    private String id;

    private String nombre;
    private String mail;
    private String password;
    private String claveAcceso;
    private String tipo;
    private String sexo;
    private boolean activo;
    private String idFacultad;
    private boolean asig;
    private Notificacion noti;
    private LocalDateTime time;

    private ZonedDateTime fcCre;
    private ZonedDateTime fcMod;

    private ZonedDateTime fcIntFal;
    private ZonedDateTime fcCamCla;
    private Integer intFal;
    private Long contSe = 0L;
    private ZonedDateTime fcLastLogin;
    private ZonedDateTime fcLastLoginOld;

    public Usuario(Usuario user) {
        this.id = user.id;
        this.nombre = user.nombre;
        this.mail = user.mail;
        this.password = user.password;
        this.claveAcceso = user.claveAcceso;
        this.tipo = user.tipo;
        this.sexo = user.sexo;
        this.activo = user.activo;
        this.idFacultad = user.idFacultad;
        this.fcCre = user.fcCre;
        this.fcMod = user.fcMod;
        this.fcIntFal = user.fcIntFal;
        this.fcCamCla = user.fcCamCla;
        this.intFal = user.intFal;
        this.contSe = user.contSe;
        this.fcLastLogin = user.fcLastLogin;
        this.fcLastLoginOld = user.fcLastLoginOld;

        if (intFal == null) {
            this.intFal = 0;
        }
    }

}
