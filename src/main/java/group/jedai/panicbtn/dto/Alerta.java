package group.jedai.panicbtn.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Alerta implements Serializable {

    @Id
    private String id;
    private String idUsuario;
//    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
//    @JsonSerialize(using = DateSerializer.class)
    private LocalDateTime fcAlerta;
    private Double latitude;
    private Double longitude;
    private Double latitudeG;
    private Double longitudeG;
    private String idGuardia;
    private LocalDateTime fcInicio;
    private LocalDateTime fcFin;
    private boolean activo;

}
