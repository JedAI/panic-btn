package group.jedai.panicbtn.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Notificacion implements Serializable {
    @Id
    private String id;
    private String idUsuario;
    private LocalDateTime fcAlerta;
    private Double latitude;
    private Double longitude;
}
