package group.jedai.panicbtn.dto;

import group.jedai.panicbtn.util.Ctns;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CustomUserDetails extends Usuario implements UserDetails {

    private static final long serialVersionUID = 1L;

    private List<GrantedAuthority> authorities;

    public CustomUserDetails(Usuario user) {
        super(user);
        List<String> userRoles = new ArrayList<>();
        userRoles.add("ROLE_USER");
        String permisos = StringUtils.collectionToCommaDelimitedString(userRoles);
        authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(permisos);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getUsername() {
        return getMail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Si el numero de intentos es superior al permitido y el tiempo desde el ultimo login fallido
     * no supera el tiempo permitido, la cuenta esta bloqueada
     *
     * @return true si la cuenta esta bloqueada
     */
    @Override
    public boolean isAccountNonLocked() {
        if (getFcIntFal() != null && getIntFal() >= Ctns.MAX_LOGIN_FAIL) {
            long diff = getFcIntFal().until(LocalDateTime.now(), ChronoUnit.MINUTES);
            return diff >= Ctns.LOGIN_RETRY_TIME;
        } else {
            return getIntFal() == 0 || this.getIntFal() < Ctns.MAX_LOGIN_FAIL;
        }
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.getIntFal() < Ctns.MAX_LOGIN_FAIL;
    }

    @Override
    public boolean isEnabled() {
        return this.isActivo();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof CustomUserDetails && getUsername().equals(((CustomUserDetails) o).getUsername());
    }

    @Override
    public int hashCode() {
        return getUsername().hashCode();
    }
}
