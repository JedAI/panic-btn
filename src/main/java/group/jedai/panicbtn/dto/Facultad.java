package group.jedai.panicbtn.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class Facultad implements Serializable {

    private String id;
    private String nombre;

}
