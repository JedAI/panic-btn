package group.jedai.panicbtn.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NivelServicio implements Serializable {
    @Id
    private String id;
    @Indexed
    private String idA;//id alerta
    private String m; //motivo de alerta
    private String na; //nivel de atencion
    private String obs; //observacion
}
