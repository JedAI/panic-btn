package group.jedai.panicbtn.sec;

import group.jedai.panicbtn.srv.LoginSrv;
import group.jedai.panicbtn.util.Ctns;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.session.HttpSessionEventPublisher;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${app.login.admin.username}")
    String username;
    @Value("${app.login.admin.password}")
    String password;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/error", "/login**", "/actuator/**", "/api/movil/**", "/test**", "/test/**", "ws**", "/ws**", "/ws/**", "/topic**", "/app**").permitAll()
                .antMatchers("/adm/**").hasRole("SUPER_ADMIN")
                .anyRequest().authenticated()

                .and().cors().and()

                .logout()
                .invalidateHttpSession(true)
                .logoutSuccessUrl(Ctns.LOGIN_URL)
                .permitAll()

                .and()

                .sessionManagement()//https://github.com/spring-projects/spring-boot/issues/1537
                .maximumSessions(2)
                .maxSessionsPreventsLogin(false)
                .expiredUrl("/expired")
                .sessionRegistry(sessionRegistry())
                .and()
                .and()
                .exceptionHandling().authenticationEntryPoint(customAuthenticationEntryPoint())
                .and()

                .exceptionHandling()
                .accessDeniedPage("/error")

                .and().csrf()
                .ignoringAntMatchers("/api/**", "/**")
                .and()
                //.httpBasic().and()
                .addFilterAt(customBasicFilter(), BasicAuthenticationFilter.class);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth, LoginSrv userDetailsService) throws Exception {

        auth
                .userDetailsService(userDetailsService)
                //.passwordEncoder(passwordEncoder())
                .and()
                .inMemoryAuthentication().passwordEncoder(passwordEncoder())
                .withUser(username).password(password).roles("SUPER_ADMIN", "ADMIN", "ACTUATOR").and();
        //.passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    // Work around https://jira.spring.io/browse/SEC-2855
    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    // Register HttpSessionEventPublisher
    @Bean
    public static ServletListenerRegistrationBean httpSessionEventPublisher() {
        return new ServletListenerRegistrationBean<>(new HttpSessionEventPublisher());
    }

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    @Bean
    CustomAuthenticationEntryPoint customAuthenticationEntryPoint() throws Exception {
        return new CustomAuthenticationEntryPoint();
    }

    @Bean
    CustomBasicFilter customBasicFilter() throws Exception {
        return new CustomBasicFilter(authenticationManager(), customAuthenticationEntryPoint());
    }

}
