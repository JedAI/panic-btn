package group.jedai.panicbtn.srv;

import group.jedai.panicbtn.dto.Usuario;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Service
public class EmailService {

    private final JavaMailSender mail;
    @Value("${spring.mail.username}")
    private String FROM_EMAIL;

    public EmailService(JavaMailSender mail) {
        this.mail = mail;
    }

    private String buildHtmlMessage(Usuario usuario) {
        String message = "<h1> SmartAlert-UCE</h1>"
                + "<h2> Clave de verificacion </h2>"
                + "<h3> Estimado: "
                + " <strong>" + usuario.getNombre() + "</strong> "
                + "</h3>"
                + "<br>"
                + "<h3> Reciba la bienvenida a SmartAlert-UCE </h3>"
                + "<h4> su clave de verificacion es: " + usuario.getClaveAcceso() + " </h4>"
                + "<br>"
                + "<br>"
                + "<p>\n Este email ha sido generado exlusivamente para su destinatario, no reenvie o imprima este email ni comparta con nadie, es de estricta confidencialidad entre usted y SmartAlert-UCE </p>\n"
                + "<p>\n No responda a este email ha sido generado automaticamente. </p>";

        return message;
    }

    public void sendEmail(Usuario usuario) {

        MimeMessagePreparator preparator = mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "utf-8");
            message.setTo(usuario.getMail());
            message.setSubject("Notificacion Clave de Validacion");
            message.setFrom("smartalert@gmoncayoresearch.com");
            mimeMessage.setContent(buildHtmlMessage(usuario), "text/html");
        };
        this.mail.send(preparator);
    }

    private String buildHtmlMessagePass(Usuario usuario) {
        String message = "<h1> SmartAlert-UCE </h1>"
                + "<h2> Nuevo Password </h2>"
                + "<h3> Estimado: "
                + " <strong>" + usuario.getNombre() + "</strong> "
                + "</h3>"
                + "<br>"
                + "<h3> Reciba la bienvenida a SmartAlert-UCE </h3>"
                + "<h4> su Password es: " + usuario.getPassword() + " </h4>"
                + "<br>"
                + "<br>"
                + "<p>\n Este email ha sido generado exlusivamente para su destinatario, no reenvie o imprima este email ni comparta con nadie, es de estricta confidencialidad entre usted y SmartAlert-UCE </p>\n"
                + "<p>\n No responda a este email ha sido generado automaticamente. </p>";

        return message;
    }

    public void sendEmailPass(Usuario usuario) {

        MimeMessagePreparator preparator = mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "utf-8");
            message.setTo(usuario.getMail());
            message.setSubject("Notificacion Nuevo Password");
            message.setFrom("smartalert@gmoncayoresearch.com");
            mimeMessage.setContent(buildHtmlMessagePass(usuario), "text/html");
        };
        this.mail.send(preparator);
    }

    private String buildHtmlMessageEncuesta(Usuario usuario) {
        String message = "<h1>Encuesta SmartAlert-UCE </h1>"
                + "<h2></h2>"
                + "<h3> Estimado: "
                + " <strong>" + usuario.getNombre() + "</strong> "
                + "</h3>"
                + "<h3> Reciba un cordial saludo de parte de SmartAlert-UCE </h3>"
                + "<br>"
                + "<h4> Por favor ingrese a este link: https://forms.gle/dQ5tUrLWF6q5ZBmt5 el cual nos ayudara a recolectar informacion necesaria " +
                "para culminar este proyeco. </h4>"
                + "<br>"
                + "<h4> De ante mano queremos agradecer su colaboracion en este proyecto. </h4>"
                + "<br>"
                + "<br>"
                + "<p>\n Este email ha sido generado exlusivamente para su destinatario, no reenvie o imprima este email ni comparta con nadie, es de estricta confidencialidad entre usted y SmartAlert-UCE </p>\n"
                + "<p>\n No responda a este email ha sido generado automaticamente. </p>";

        return message;
    }

    public void sendEmailEncuesta(Usuario usuario) {

        MimeMessagePreparator preparator = mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "utf-8");
            message.setTo(usuario.getMail());
            message.setSubject("Encuesta SmartAlert-UCE");
            message.setFrom("smartalert@gmoncayoresearch.com");
            mimeMessage.setContent(buildHtmlMessageEncuesta(usuario), "text/html");
        };
        this.mail.send(preparator);
    }

}
