package group.jedai.panicbtn.srv;

import com.fasterxml.jackson.databind.node.ObjectNode;
import group.jedai.panicbtn.exc.CustomException;
import group.jedai.panicbtn.util.Marker;
import group.jedai.panicbtn.dao.AlertaDAO;
import group.jedai.panicbtn.dao.UsuarioDAO;
import group.jedai.panicbtn.dto.Alerta;
import group.jedai.panicbtn.dto.Usuario;
import group.jedai.panicbtn.util.UserProximo;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.DecimalFormat;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class AlertaSrv {
    private final AlertaDAO alertaDAO;
    private final UsuarioDAO usuarioDAO;
    private final MongoTemplate mongoTemplate;
    private final SimpMessagingTemplate template;

    public AlertaSrv(AlertaDAO alertaDAO, UsuarioDAO usuarioDAO, MongoTemplate mongoTemplate, SimpMessagingTemplate template) {
        this.alertaDAO = alertaDAO;
        this.usuarioDAO = usuarioDAO;
        this.mongoTemplate = mongoTemplate;
        this.template = template;
    }

    /**
     * añadir una alerta se verifica que el usuario pueda enviar notificaciones, si el usuario puede enviar
     * notificaciones se le crea la fcInicio con la hora exacta para ser guardad, se le asignara una guardia
     * quien acudira en auxilio si ya tiene asignado solo guardare la alerta.
     *
     * @param alerta alerta emitida por aparte de los usuarios
     * @return la alerta
     */
    public Alerta add(Alerta alerta) throws InterruptedException {
        System.out.println("entra: " + alerta);
        if (!StringUtils.isEmpty(alerta.getId())) {
            var alerta2 = alertaDAO.findById(alerta.getId()).orElse(null);
            if (alerta2 != null) {
                alerta.setIdGuardia(alerta2.getIdGuardia());
                alerta.setFcInicio(alerta2.getFcInicio());
            }
        } else {
            //me esta llegando una nueva alerta
            var aler = alertaDAO.findByIdUsuarioAndActivo(alerta.getIdUsuario(), true);
            if (aler != null) {
                throw new CustomException("error");
            } else {
                alerta = alertaDAO.save(alerta);
            }
        }
        if (!alerta.isActivo()) {
            alerta.setFcFin(LocalDateTime.now());
            alerta.setActivo(false);
            if (alerta.getIdGuardia() != null) {
                Usuario guar = usuarioDAO.findById(alerta.getIdGuardia()).orElse(null);
                if (guar != null) {
                    guar.setAsig(false);
                    usuarioDAO.save(guar);
                }
            }
            return alertaDAO.save(alerta);
        }
        alerta.setFcInicio(alerta.getFcInicio() != null ? alerta.getFcInicio() : LocalDateTime.now());
        Optional<Usuario> usuario = usuarioDAO.findById(alerta.getIdUsuario());
        if (usuario.isPresent() && usuario.get().isActivo()) {
            //enviar a que se imprima en el mapa
            var mar = Marker.builder()
                    .id(usuario.get().getId())
                    .latitude(alerta.getLatitude())
                    .longitude(alerta.getLongitude())
                    .tipo(usuario.get().getTipo())
                    .nm(usuario.get().getNombre())
                    .idG("Sin Asignar")
                    .nmG("Sin Asignar")
                    .build();
            if (alerta.getIdGuardia() != null) {
                var usr = usuarioDAO.findById(alerta.getIdGuardia()).orElse(null);
                if (usr != null) {
                    mar.setIdG(usr.getId());
                    mar.setNmG(usr.getNombre());
                }
            }
            template.convertAndSend("/topic/point_update", mar);
            if (alerta.getIdGuardia() == null) { //verifico que no tenga un guardia asignado para asignarlo
                String asig = asignacion(alerta);
                if (asig != null) {
                    //avisar al guardia
                    var usr = usuarioDAO.findById(alerta.getIdGuardia()).orElse(null);
                    if (usr != null) {
                        alerta.setLatitudeG(usr.getNoti().getLatitude());
                        alerta.setLongitudeG(usr.getNoti().getLongitude());
                        mar.setIdG(usr.getId());
                        mar.setNmG(usr.getNombre());
                    }
                    template.convertAndSend("/topic/" + asig, alerta);
                    System.out.println("guardia: " + asig + " asignado a: " + usuario.get().getNombre());
                    template.convertAndSend("/topic/point_update", mar);
                    return alertaDAO.save(alerta);
                } else {
                    //guardar la alerta sin un guardia asignado
                    return alertaDAO.save(alerta);
                }
            } else {//solo guardo la alerta con el id de guardia que es enviado
                var usr = usuarioDAO.findById(alerta.getIdGuardia()).orElse(null);
                if (usr != null) {
                    alerta.setLatitudeG(usr.getNoti().getLatitude());
                    alerta.setLongitudeG(usr.getNoti().getLongitude());
                }
                template.convertAndSend("/topic/" + alerta.getIdGuardia(), alerta);
                return alertaDAO.save(alerta);
            }
        } else {
            System.out.println("el usuario no puede emitir alertas");
            return null;
        }
    }

    public List<Alerta> findAlertas() {
        return alertaDAO.findAll();
    }

    /**
     * Metodo para determinar que guardia puedo asignar a la alerta determinado por la distancia
     * emitida a la de los distintos guardias y que no tengan asignado una alerta anteriormente
     *
     * @param alerta alerta emtidia
     * @return id del guardia hacer asignado si no se pudo retorna nulo
     */
    private String asignacion(Alerta alerta) throws InterruptedException {
        // send websocket en el telefono
        Thread.sleep(5000);
        List<Usuario> notificacions = notificacionList();
        List<UserProximo> proximos = new ArrayList<>();
        notificacions.forEach(n -> proximos.add(UserProximo.builder().id(n.getId())
                .dist(distancia(alerta.getLatitude(), alerta.getLongitude()
                        , n.getNoti().getLatitude(), n.getNoti().getLongitude()))
                .build()));
        proximos.sort(UserProximo::compareTo);
        Usuario usr;
        for (UserProximo p : proximos) {
            usr = usuarioDAO.findByIdAndAsig(p.getId(), false);
            if (usr != null) {
                usr.setAsig(true);
                usuarioDAO.save(usr);
                alerta.setLatitudeG(usr.getNoti().getLatitude());
                alerta.setLongitudeG(usr.getNoti().getLongitude());
                alerta.setIdGuardia(usr.getId());
                return p.getId();
            }
            System.out.println("no se asigno el guardia " + p.getId() + " por estar ocupado.");
        }
        return null;
    }

    /**
     * Metodo que devuelve las alertas emitidas por fecha
     *
     * @param item a qui debe contener el atirubuto fecha para hacer consultada
     * @return lista de alertas emitidas en el dia
     */
    @SuppressWarnings("Duplicates")
    public Map<String, Object> findByFc(ObjectNode item) {
        LocalDateTime fc = LocalDateTime.ofInstant(Instant.parse(item.get("fecha").asText()), ZoneId.of(ZoneOffset.UTC.getId()));
        LocalDateTime startDay = LocalDate.parse(fc.toLocalDate().toString()).atStartOfDay(ZonedDateTime.now().getZone()).toLocalDate().atStartOfDay();
        LocalDateTime endDay = LocalDate.parse(fc.toLocalDate().toString()).atTime(23, 59, 59);
        Criteria criteria = where("fcInicio").gte(startDay).lte(endDay).and("activo").is(false);
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(criteria),
                Aggregation.sort(Sort.Direction.ASC, "fcAlerta")
        );
        AggregationResults<Alerta> aggregationResults = mongoTemplate.aggregate(aggregation, Alerta.class, Alerta.class);
        List<Alerta> alertas = aggregationResults.getMappedResults();
        Map<String, Object> map = new HashMap<>();
        map.put("alertas", alertas);
        return map;
    }

    /**
     * Metodo que devuelve las alertas emitidas en total
     *
     * @return numero de alertas emitidas
     */
    private Map<String, Object> countAlertaTotal() {
        Criteria criteria = where("activo").is(false).and("fcInicio").exists(true).and("fcFin").exists(true);
        Query query = query(criteria);
        List<Alerta> alertas = mongoTemplate.find(query, Alerta.class);
        var stats = alertas.stream().mapToLong(a -> ChronoUnit.MINUTES.between(a.getFcInicio(), a.getFcFin())).summaryStatistics();
        Integer usuarios = usuarioDAO.findAll().size();
        Map<String, Object> map = new HashMap<>();
        map.put("alertas", alertas.size());
        map.put("promedio", new DecimalFormat("#0.00").format(stats.getAverage()));
        map.put("registrados", usuarios);
        return map;
    }

    public Map<String, Object> count() {
        LocalDateTime fc = LocalDateTime.now();
        Integer year = fc.getYear();
        int month = fc.getMonthValue();
        Integer alertasMes = 0;
        Integer[] alertas = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (int i = 0; i < month; i++) {
            Map<String, Object> tmp = countAlertaMes(year, i + 1);
            Integer tmpT = (Integer) tmp.get("alertas");
            alertas[i] = tmpT;
            alertasMes = tmpT;
        }
        Map<String, Object> map = new HashMap<>();
        map.put("alertas", alertas);
        map.put("alertasMes", alertasMes);
        map.put("recibidas", countAlertaTotal());
        return map;
    }

    /**
     * metodo que devuelve las alertas por mes dado
     *
     * @param year  año de busqueda
     * @param month mes de busqueda
     * @return cantidad de alertas en ese mes
     */
    @SuppressWarnings("Duplicates")
    private Map<String, Object> countAlertaMes(Integer year, Integer month) {
        LocalDateTime fcI = LocalDateTime.of(LocalDate.of(year, month, 1), LocalTime.now());
        LocalDateTime fcF = fcI.with(TemporalAdjusters.lastDayOfMonth());
        LocalDateTime startDay = LocalDate.parse(fcI.toLocalDate().toString()).atStartOfDay(ZonedDateTime.now().getZone()).toLocalDate().atStartOfDay();
        LocalDateTime endDay = LocalDate.parse(fcF.toLocalDate().toString()).atTime(23, 59, 59);
        Criteria criteria = where("activo").is(false).and("fcInicio").gte(startDay).lte(endDay).and("fcFin").exists(true);
        Query query = query(criteria);
        List<Alerta> alertas = mongoTemplate.find(query, Alerta.class);
        Map<String, Object> map = new HashMap<>();
        map.put("alertas", alertas.size());
        return map;
    }

    /**
     * Metodo que devuelve alertas emitidas por el un usario con una fecha de inicio y un guardia asignado
     *
     * @param item debe contener fecha inicio de la alerta y el idUsuario
     * @return lista de alertas emitidas
     */
    @SuppressWarnings("Duplicates")
    public Map<String, Object> alrtByFcIByIdUs(ObjectNode item) {
        String idU = item.get("id").asText();
        LocalDateTime fcI = LocalDateTime.parse(item.get("fecha").asText());
        Criteria criteria = where("idUsuario").is(idU).and("fcInicio").is(fcI);
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(criteria),
                Aggregation.sort(Sort.Direction.ASC, "fcAlerta")
        );
        AggregationResults<Alerta> aggregationResults = mongoTemplate.aggregate(aggregation, Alerta.class, Alerta.class);
        List<Alerta> alertas = aggregationResults.getMappedResults();
        List<Marker> markers = new ArrayList<>();
        alertas.forEach(a -> usuarioDAO.findById(a.getIdUsuario()).ifPresent(usr -> markers.add(Marker.builder().id(a.getIdUsuario()).latitude(a.getLatitude()).longitude(a.getLongitude())
                .nm(usr.getNombre()).tipo(usr.getTipo()).build())));
        Map<String, Object> map = new HashMap<>();
        map.put("alertas", alertas);
        map.put("markers", markers);
        return map;
    }


    /**
     * Metodo para determinar la distancia entre el punto donde se lanzo la alerta y el
     * punto donde esta el guardia
     *
     * @param lat1 latitud de la alerta
     * @param lng1 longitud de la alerta
     * @param lat2 latitud de la notificacion
     * @param lng2 longitud de la notificacion
     * @return la distancia entre los 2 puntos
     */
    private double distancia(double lat1, double lng1, double lat2, double lng2) {
        double radioTierra = 6371;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
        return radioTierra * va2;
    }

    //cargo la lista de notificaciones para ser comparada
    private List<Usuario> notificacionList() {
        LocalDateTime now = LocalDateTime.now();
        Criteria criteria = where("time").gte(now.minusMinutes(5)).lte(now).and("tipo").is("GUARDIA");
        Query query = query(criteria);
        return mongoTemplate.find(query, Usuario.class);
    }
}
