package group.jedai.panicbtn.srv;

import com.fasterxml.jackson.databind.node.ObjectNode;
import group.jedai.panicbtn.util.Marker;
import group.jedai.panicbtn.dao.NotificacionDAO;
import group.jedai.panicbtn.dao.UsuarioDAO;
import group.jedai.panicbtn.dto.Notificacion;
import group.jedai.panicbtn.dto.Usuario;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class NotificacionSrv {

    private final NotificacionDAO notificacionDAO;
    private final UsuarioDAO usuarioDAO;
    private final MongoTemplate mongoTemplate;
    private final SimpMessagingTemplate template;

    public NotificacionSrv(NotificacionDAO notificacionDAO, UsuarioDAO usuarioDAO, MongoTemplate mongoTemplate, SimpMessagingTemplate template) {
        this.notificacionDAO = notificacionDAO;
        this.usuarioDAO = usuarioDAO;
        this.mongoTemplate = mongoTemplate;
        this.template = template;
    }

    public Notificacion add(Notificacion notificacion) {
        Usuario usuario = usuarioDAO.findById(notificacion.getIdUsuario()).orElse(null);
        template.convertAndSend("/topic/point_update",
                Marker.builder()
                        .id(notificacion.getIdUsuario())
                        .latitude(notificacion.getLatitude())
                        .longitude(notificacion.getLongitude())
                        .tipo(usuario.getTipo())
                        .idG("Sin Asignar")
                        .nmG("Sin Asignar")
                        .nm(usuario.getNombre())
                        .build());
        usuario.setNoti(notificacion);
        usuario.setTime(LocalDateTime.now());
        notificacionDAO.save(notificacion);
        usuarioDAO.save(usuario);
        return notificacion;
    }

    //lista para ver que guardias estan activos o no
    public void posicionActualG() {
        System.out.println("consultando posiciones actuales");
        LocalDateTime now = LocalDateTime.now();
        Criteria criteria = where("time").gte(now.minusMinutes(5)).lte(now).and("tipo").is("GUARDIA");
        Query query = query(criteria);
        List<Usuario> list = mongoTemplate.find(query, Usuario.class);
        list.forEach(usuario -> {
            template.convertAndSend("/topic/point_update",
                    Marker.builder()
                            .id(usuario.getId())
                            .latitude(usuario.getNoti().getLatitude())
                            .longitude(usuario.getNoti().getLongitude())
                            .tipo(usuario.getTipo())
                            .idG("Sin Asignar")
                            .nmG("Sin Asignar")
                            .nm(usuario.getNombre()).build());
        });
        System.out.println("ubicaciones: " + list);
    }

}
