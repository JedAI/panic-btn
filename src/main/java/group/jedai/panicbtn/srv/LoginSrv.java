package group.jedai.panicbtn.srv;

import group.jedai.panicbtn.dto.CustomUserDetails;
import group.jedai.panicbtn.dto.Usuario;
import group.jedai.panicbtn.util.Ctns;
import group.jedai.panicbtn.util.LicenseAuthenticationException;
import lombok.Data;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Data
@Service
public class LoginSrv implements UserDetailsService {

    private final Log log = LogFactory.getLog(getClass());

    private final UsuarioSrv usuarios;
    private final SessionRegistry sessionRegistry;

    private boolean invalidateEnable;

    @Autowired
    public LoginSrv(UsuarioSrv usuarios, SessionRegistry sessionRegistry) {
        this.usuarios = usuarios;
        this.sessionRegistry = sessionRegistry;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario clt = usuarios.loadUserByUsername(username).orElse(null);
        if (null == clt) {
            log.debug("No user present with username: " + username);
            throw new UsernameNotFoundException("No user present with username: " + username);
        } else {
            return new CustomUserDetails(clt);
        }
    }

    /**
     * Se ejecuta cuando el login ha fallado, dependiendo del motivo de fallo del login
     * muestra un mensaje, y llama al servivio para actualizar el numero de logins fallidos
     *
     * @param username usuario q intento iniciar sesion
     * @param ex       tipo de excepcion x la q no pudo iniciar sesion
     */
    public String failLoginUser(String username, AuthenticationException ex) {
        String classEx = ex.getClass().getName();
        log.debug(classEx);
        String errorMessage;
        if (classEx.equalsIgnoreCase(org.springframework.security.authentication.BadCredentialsException.class.getName())) {
            errorMessage = "Usuario o contraseña incorrecto";

            if (usuarios.failLoginUser(username)) {
                errorMessage = "Ha excedido el número de intentos, espere " + Ctns.LOGIN_RETRY_TIME + "minutos y reintente";
            }
        } else if (classEx.equalsIgnoreCase(org.springframework.security.authentication.DisabledException.class.getName())) {
            if (usuarios.accountDisabled(username)) {
                errorMessage = "Es la primera vez q intenta iniciar sesión, se ha enviado un email con su clave de acceso";
            } else {
                errorMessage = "Cuenta inabilitada o no confirmada";
            }
        } else if (classEx.equalsIgnoreCase(org.springframework.security.authentication.LockedException.class.getName())) {
            long time = usuarios.accountLocked(username);
            if (time > 0) {
                errorMessage = "Ha excedido el número de intentos, espere " + time + "minutos y reintente";
            }
            errorMessage = "Cuenta bloqueda, ha excedido el numero de intentos";
        } else if (classEx.equalsIgnoreCase(org.springframework.security.web.authentication.session.SessionAuthenticationException.class.getName())) {
            errorMessage = "Usuario ha superado el número de sesiones que se les permite tener simultáneamente";
            invalidateEnable = true;
            expireUserSessions(username);
        }
        if (classEx.equalsIgnoreCase(LicenseAuthenticationException.class.getName())) {
            errorMessage = ex.getMessage();
        } else {
            errorMessage = "Usuario o contraseña incorrecto";
        }

        return errorMessage;
    }

    /**
     * cuando el usuario ha inciado correctamente la session, se ejecuta este metodo llamado desde
     *
     * @param username usuario q inico sesion
     */

    public Usuario successLogin(String username) {
        return usuarios.successLogin(username);
    }

    private void expireUserSessions(String username) {
        for (Object principal : sessionRegistry.getAllPrincipals()) {
            if (principal instanceof UserDetails) {
                UserDetails userDetails = (UserDetails) principal;
                if (userDetails.getUsername().equals(username)) {
                    for (SessionInformation information : sessionRegistry.getAllSessions(userDetails, true)) {
                        information.expireNow();
                    }
                }
            }
        }
    }

}
