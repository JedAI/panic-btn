package group.jedai.panicbtn.srv;

import group.jedai.panicbtn.dao.UsuarioDAO;
import group.jedai.panicbtn.dto.Usuario;
import group.jedai.panicbtn.exc.CustomException;
import group.jedai.panicbtn.util.Ctns;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class UsuarioSrv {

    private final UsuarioDAO usuarioDAO;
    private final EmailService emailService;
    private final SimpMessagingTemplate template;

    @Value("${app.files.fotos}")
    public String FOTOS_FOLDER = "./";

    public UsuarioSrv(UsuarioDAO usuarioDAO, EmailService emailService, SimpMessagingTemplate template) {
        this.usuarioDAO = usuarioDAO;
        this.emailService = emailService;
        this.template = template;
    }

    public Usuario add(Usuario usuario) {
        if (!usuario.getMail().contains("@uce.edu.ec")) {
            throw new CustomException("No puede utilizar ese correo electrónico para su registro");
        }
        Usuario ret;
        Usuario u = usuarioDAO.findByMail(usuario.getMail());
        if (u == null) {
            usuario.setTipo(usuario.getTipo().toUpperCase());
            usuario.setClaveAcceso(org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric(8));
            emailService.sendEmail(usuario);
            usuarioDAO.save(usuario);
            ret = usuario;
        } else {
            ret = null;
            System.out.println("El mail ya se encuentra registrado");
        }
        return ret;
    }

    public Usuario activar(String mail, String claveAcceso) {
        Usuario usuario = usuarioDAO.findByMailAndClaveAcceso(mail, claveAcceso);
        if (usuario != null) {
            usuario.setActivo(true);
            usuarioDAO.save(usuario);
            return usuario;
        } else {
            System.out.println("No se puede activar al usuario");
            return null;
        }
    }

    public List<Usuario> usuarioList() {
        return usuarioDAO.findAll();
    }

    public void enviarEncuesta(boolean enviar) {
        if (enviar) {
            var list = usuarioDAO.findAll();
            list.forEach(emailService::sendEmailEncuesta);
        }
    }

    /**
     * busca los usuarios que sean del mismo tipo guardia
     *
     * @return todos los usuarios que son guardias
     */
    public List<Usuario> usuarioListNotificacion() {
        return usuarioDAO.findByTipo("ESTUDIANTE");
    }

    public Usuario usuarioById(String id) {
        return usuarioDAO.findById(id).orElse(null);
    }

    public Usuario findUserByMail(String mail) {
        return usuarioDAO.findByMail(mail);
    }

    public Usuario findUserLogin(String mail, String password) {
        Usuario usuario = usuarioDAO.findByMailAndPassword(mail, password);
        if (usuario != null) {
            usuario.setTime(null);
            usuario.setNoti(null);
            return usuario;
        } else {
            return null;
        }
    }

    public Usuario resetClave(String mail) {
        Usuario ret;
        Usuario u = usuarioDAO.findByMail(mail);
        if (!StringUtils.isEmpty(u)) {
            u.setClaveAcceso(org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric(8));
            emailService.sendEmail(u);
            ret = usuarioDAO.save(u);
        } else {
            ret = null;
            System.out.println("El mail no se encuentra registrado");
        }
        return ret;
    }

    public Usuario resetPass(String mail) {
        Usuario ret;
        Usuario u = usuarioDAO.findByMail(mail);
        if (!StringUtils.isEmpty(u)) {
            u.setPassword(org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric(8));
            emailService.sendEmailPass(u);
            ret = usuarioDAO.save(u);
        } else {
            ret = null;
            System.out.println("El mail no se encuentra registrado");
        }
        return ret;
    }

    public String cambiarPass(List<String> user) throws CustomException {
        String ret = null;
        Usuario us = usuarioDAO.findById(user.get(0)).orElse(null);

        if (us != null) {
            final String s = "";
            String newPass = user.get(2);
            if (!(user.get(1)).equals(us.getPassword())) {
                ret = "Contraseña actual incorrecta";
            } else if ((user.get(2)).equals(us.getPassword())) {
                ret = "Su nueva contraseña no puede ser la misma";
            } else if (user.get(2).equals(user.get(3))) {
                us.setPassword(newPass);
                usuarioDAO.save(us);
                ret = "Contraseña cambiada";
            } else {
                ret = "Las contraseñas no coinciden";
            }
        } else {
            return "Error";
        }
        return ret;
    }

    public String savePhoto(byte[] data, String id) throws IOException {
        Usuario usuario = usuarioDAO.findById(id.toString()).orElse(null);
        if (usuario != null) {
            File file = new File(FOTOS_FOLDER + usuario.getId() + ".jpg");
            file.mkdirs();
            Files.copy(new ByteArrayInputStream(data), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            return "guardado";
        } else {
            return "fallo";
        }
    }

    /**
     * el usuario coloco mal su contraseña entonces debe aumentarse el contador de inicios de sesion
     * fallidos, y actualizar la fecha de inicios de sesion fallidos
     *
     * @param username usuario q fallo en inio de sesioón
     * @return true or false
     */
    boolean failLoginUser(String username) {
        Usuario u = usuarioDAO.findByMail(username);
        if (u != null) {
            int loginFails = u.getIntFal() == null ? 0 : u.getIntFal();
            u.setIntFal(loginFails + 1);
            u.setFcIntFal(ZonedDateTime.now());
            usuarioDAO.save(u);
            return u.getIntFal() >= 3;

        }
        return false;
    }

    /**
     * cuando el usuario ha inciado correctamente la session
     * entonces debemos poner en cero los login fallidos
     * y nullificar la fecha de ultimo inicio sesion fallido
     *
     * @param username usuario q inciio sesion correctamente
     * @return usuario q inicio sesion
     */
    Usuario successLogin(String username) {
        Usuario u = usuarioDAO.findByMail(username);
        if (u != null) {
            u.setFcIntFal(null);
            u.setIntFal(0);
            u.setFcLastLoginOld(u.getFcLastLogin());
            u.setFcLastLogin(ZonedDateTime.now());
            u.setContSe(u.getContSe() + 1);
            usuarioDAO.save(u);
            return u;
        }
        return null;
    }

    /**
     * cuando se solicita una nueva contraseña, entonces se genera una automaticamente
     * se la envia por email al usuario, y se encera y nulifica, los contadores y fecha de logins fallidos
     *
     * @param username q solicito el cambio de contraseña
     * @return true sies q se cambio exitosamente
     */
    public boolean resetearPassword(String username) {
        Optional<Usuario> _u = Optional.ofNullable(usuarioDAO.findByMail(username));
        if (_u.isPresent()) {
            var u = _u.get();
            u.setIntFal(0);
            u.setActivo(true);
            u.setFcIntFal(null);
            String tmpPassword = org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric(8);
            u.setPassword(new BCryptPasswordEncoder().encode(tmpPassword));
            //emailService.sendNewPasswordEmail(u, "", "", tmpPassword);
            u.setFcCamCla(ZonedDateTime.now());
            usuarioDAO.save(u);
            return true;
        }
        return false;
    }

    /**
     * cuando el usuario colocó la contraseña correcta pero la cuenta esta bloqueada
     * porque el tiempo desde el ultimo intento fallido de inicio de sesion no supera
     * el tiempo permitido
     *
     * @param username usuario cuya cuenta esta bloqueada
     * @return tiempo q debe esperar para volver a intentar iniciar sesión
     */
    long accountLocked(String username) {
        Usuario u = usuarioDAO.findByMail(username);

        if (u != null && u.getFcIntFal() != null) {
            long tiempoDesdeUltimoFallo = u.getFcIntFal().until(LocalDateTime.now(), ChronoUnit.MINUTES);
            return (Ctns.LOGIN_RETRY_TIME - tiempoDesdeUltimoFallo);
        }
        return 0;
    }

    public void cambiarPassword(String currentPassword, String pwd1, String pwd2, String username) {
        Optional<Usuario> _u = Optional.ofNullable(usuarioDAO.findByMail(username));

        _u.ifPresent(u -> {
            BCryptPasswordEncoder enc = new BCryptPasswordEncoder();
            String newPass = enc.encode(pwd1);
            if (!enc.matches(currentPassword, u.getPassword())) {
                throw new CustomException("Contraseña actual incorrecta");
            } else if (enc.matches(pwd1, u.getPassword())) {
                throw new CustomException("Contraseña nueva no puede ser la misma.");
            } else if (pwd1.equals(pwd2)) {
                u.setPassword(newPass);
                u.setFcCamCla(ZonedDateTime.now());
                usuarioDAO.save(u);
            } else {
                throw new CustomException("Contraseña nueva no coiniciden entre si.");
            }

        });
    }


    /**
     * Cuando un usuario intentó inciar sesión pero la cuenta está deshabilitada
     * si es la primera vez, generar contraseña y enviar email
     *
     * @param username usuario q intento iniciar sesión
     * @return true sies la cuenta es nueva y se genera la clave x primera vez
     */
    boolean accountDisabled(String username) {
//        Usuario u = dao.findByUsername(username).;
        return false;
    }

    private String setUsernewProps(Usuario user) {
        ZonedDateTime d = ZonedDateTime.now();
        user.setActivo(true);
        user.setFcCre(d);
        user.setIntFal(0);
        user.setFcIntFal(null);
        String tmpPassword = org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric(8);
        user.setPassword(new BCryptPasswordEncoder().encode(tmpPassword));
        user.setFcCamCla(d);
        user.setContSe(0L);
        return tmpPassword;
    }

    public void saveUsuario(Usuario user) {
        Usuario u = usuarioDAO.findByMail(user.getMail());
        if (u != null && (user.getMail() == null || StringUtils.isEmpty(user.getMail()))) {// intentanto agregar uno q ya existe
            throw new CustomException("Usuario ya existe");
        }

        ZonedDateTime d = ZonedDateTime.now();
        if (StringUtils.isEmpty(user.getMail())) { // user nuevo lo creo
            u = user;
            String tmpPassword = setUsernewProps(u);
            u.setFcCre(d);
        } else if (u != null) {// user old
            u.setFcMod(d);
            u.setActivo(user.isActivo());
        }

        usuarioDAO.save(u);
    }

    public void toggleEstado(String username) {
        Optional<Usuario> _user = Optional.ofNullable(usuarioDAO.findByMail(username));
        _user.ifPresent(user -> {
            if (user.isActivo()) {
                user.setActivo(false);
            } else {
                user.setActivo(true);
            }
            usuarioDAO.save(user);
        });
    }

    public Optional<Usuario> loadUserByUsername(String username) {
        Optional<Usuario> _user = Optional.ofNullable(usuarioDAO.findByMail(username));
        return _user;
    }

}
