package group.jedai.panicbtn.srv;

import group.jedai.panicbtn.dao.MotivoAlertaDAO;
import group.jedai.panicbtn.dto.EstadoAlerta;
import group.jedai.panicbtn.dto.MotivoAlerta;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MotivoAlertaSrv {
    private final MotivoAlertaDAO motivoAlertaDAO;

    public MotivoAlertaSrv(MotivoAlertaDAO motivoAlertaDAO) {
        this.motivoAlertaDAO = motivoAlertaDAO;
    }


    public List<String> list() {
        List<MotivoAlerta> list = motivoAlertaDAO.findAll();
        List<String> motivos = new ArrayList<>();
        list.stream().forEach(motivoAlerta -> {
            motivos.add(motivoAlerta.getNm());
        });
        return motivos;
    }

    public MotivoAlerta save(MotivoAlerta item) {
        return motivoAlertaDAO.save(item);
    }

    public MotivoAlerta findOne(String nm){
        return motivoAlertaDAO.findByNm(nm);
    }
}
