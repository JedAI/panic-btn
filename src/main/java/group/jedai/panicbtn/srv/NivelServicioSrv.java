package group.jedai.panicbtn.srv;

import com.fasterxml.jackson.databind.node.ObjectNode;
import group.jedai.panicbtn.dao.NivelServicioDAO;
import group.jedai.panicbtn.dto.Alerta;
import group.jedai.panicbtn.dto.NivelServicio;
import group.jedai.panicbtn.dto.Resul;
import group.jedai.panicbtn.dto.Usuario;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class NivelServicioSrv {
    private final NivelServicioDAO nivelServicioDAO;
    private final MongoTemplate mongoTemplate;

    public NivelServicioSrv(NivelServicioDAO nivelServicioDAO, MongoTemplate mongoTemplate) {
        this.nivelServicioDAO = nivelServicioDAO;
        this.mongoTemplate = mongoTemplate;
    }

    public List<NivelServicio> list() {
        return nivelServicioDAO.findAll();
    }

    public NivelServicio findByIdA(String idA) {
        return nivelServicioDAO.findByIdA(idA);
    }

    public List<NivelServicio> listaMotivo(String mot) {
        return nivelServicioDAO.findAllByM(mot);
    }

    public List<NivelServicio> listaNivel(String niv) {
        return nivelServicioDAO.findAllByNa(niv);
    }

    public NivelServicio save(NivelServicio item) {
        return nivelServicioDAO.save(item);
    }

    @SuppressWarnings("Duplicates")
    public Map<String, Object> colsuta(ObjectNode item) {
        LocalDateTime fi = LocalDateTime.ofInstant(Instant.parse(item.get("fi").asText()), ZoneId.of(ZoneOffset.UTC.getId()));
        LocalDateTime ff = LocalDateTime.ofInstant(Instant.parse(item.get("ff").asText()), ZoneId.of(ZoneOffset.UTC.getId()));
        LocalDateTime startDay = LocalDate.parse(fi.toLocalDate().toString()).atStartOfDay(ZonedDateTime.now().getZone()).toLocalDate().atStartOfDay();
        LocalDateTime endDay = LocalDate.parse(ff.toLocalDate().toString()).atTime(23, 59, 59);
        Criteria criteria = where("id").exists(true)
                .and("activo").is(false).and("fcAlerta").gte(startDay).lte(endDay)
                .and("fcInicio").exists(true).and("fcFin").exists(true);
        Query query = query(criteria);
        List<Alerta> alertas = mongoTemplate.find(query, Alerta.class);
        List<NivelServicio> nivelServicios = new ArrayList<>();
        List<List<Object>> es = new ArrayList<>();
        List<List<Object>> mo = new ArrayList<>();
        List<List<Object>> gen = new ArrayList<>();
        List<List<Object>> facu = new ArrayList<>();
        if (alertas.size() > 0) {
            List<String> idUs = alertas.stream().map(Alerta::getIdUsuario).collect(Collectors.toList());
            List<String> ids = alertas.stream().map(Alerta::getId).collect(Collectors.toList());
            Criteria criteria1 = where("idA").in(ids);
            Query query1 = query(criteria1);
            nivelServicios = mongoTemplate.find(query1, NivelServicio.class);
            mo = motivos(criteria1);
            es = estado(criteria1);
            gen = genero(idUs);
            facu = facultad(idUs);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("alertas", alertas);
        map.put("niveles", nivelServicios);
        map.put("estado", es);
        map.put("motivo", mo);
        map.put("genero", gen);
        map.put("facu", facu);
        map.put("recibidas", alertas.size());
        return map;
    }

    @SuppressWarnings("Duplicates")
    private List<List<Object>> motivos(Criteria criteria1) {
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(criteria1),
                Aggregation.group("m")
                        .first("m").as("label")
                        .count().as("value"));
        AggregationResults<Resul> resultadoRps = mongoTemplate.aggregate(aggregation, NivelServicio.class, Resul.class);
        List<Resul> list = resultadoRps.getMappedResults();
        List<List<Object>> resp = new ArrayList<>();
        list.forEach(o -> {
            List<Object> item = new ArrayList<>();
            item.add(o.getLabel());
            item.add(o.getValue());
            resp.add(item);
        });
        return resp;
    }

    @SuppressWarnings("Duplicates")
    private List<List<Object>> estado(Criteria criteria1) {
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(criteria1),
                Aggregation.group("na")
                        .first("na").as("label")
                        .count().as("value"));
        AggregationResults<Resul> resultadoRps = mongoTemplate.aggregate(aggregation, NivelServicio.class, Resul.class);
        List<Resul> list = resultadoRps.getMappedResults();
        List<List<Object>> resp = new ArrayList<>();
        list.forEach(o -> {
            List<Object> item = new ArrayList<>();
            item.add(o.getLabel());
            item.add(o.getValue());
            resp.add(item);
        });
        return resp;
    }

    @SuppressWarnings("Duplicates")
    private List<List<Object>> genero(List<String> ids) {
        Criteria criteria1 = where("id").in(ids);
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(criteria1),
                Aggregation.group("sexo")
                        .first("sexo").as("label")
                        .count().as("value"));
        AggregationResults<Resul> resultadoRps = mongoTemplate.aggregate(aggregation, Usuario.class, Resul.class);
        List<Resul> list = resultadoRps.getMappedResults();
        List<List<Object>> resp = new ArrayList<>();
        list.forEach(o -> {
            List<Object> item = new ArrayList<>();
            item.add(o.getLabel());
            item.add(o.getValue());
            resp.add(item);
        });
        return resp;
    }

    @SuppressWarnings("Duplicates")
    private List<List<Object>> facultad(List<String> ids) {
        Criteria criteria1 = where("id").in(ids);
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(criteria1),
                Aggregation.group("idFacultad")
                        .first("idFacultad").as("label")
                        .count().as("value"));
        AggregationResults<Resul> resultadoRps = mongoTemplate.aggregate(aggregation, Usuario.class, Resul.class);
        List<Resul> list = resultadoRps.getMappedResults();
        List<List<Object>> resp = new ArrayList<>();
        list.forEach(o -> {
            List<Object> item = new ArrayList<>();
            item.add(o.getLabel());
            item.add(o.getValue());
            resp.add(item);
        });
        return resp;
    }
}
