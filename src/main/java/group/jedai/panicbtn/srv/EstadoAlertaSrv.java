package group.jedai.panicbtn.srv;

import group.jedai.panicbtn.dao.EstadoAlertaDAO;
import group.jedai.panicbtn.dto.EstadoAlerta;
import group.jedai.panicbtn.dto.NivelServicio;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EstadoAlertaSrv {
    private final EstadoAlertaDAO estadoAlertaDAO;

    public EstadoAlertaSrv(EstadoAlertaDAO estadoAlertaDAO) {
        this.estadoAlertaDAO = estadoAlertaDAO;
    }


    public List<String> list() {
        List<EstadoAlerta> list = estadoAlertaDAO.findAll();
        List<String> estados = new ArrayList<>();
        list.stream().forEach(estadoAlerta -> {
            estados.add(estadoAlerta.getNm());
        });
        return estados;
    }

    public EstadoAlerta save(EstadoAlerta item) {
        return estadoAlertaDAO.save(item);
    }

    public EstadoAlerta findOne(String nm){
        return estadoAlertaDAO.findByNm(nm);
    }
}
