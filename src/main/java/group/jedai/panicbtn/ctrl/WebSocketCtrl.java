package group.jedai.panicbtn.ctrl;

import group.jedai.panicbtn.dto.Alerta;
import group.jedai.panicbtn.dto.Notificacion;
import group.jedai.panicbtn.util.Marker;
import group.jedai.panicbtn.srv.AlertaSrv;
import group.jedai.panicbtn.srv.NotificacionSrv;
import group.jedai.panicbtn.srv.UsuarioSrv;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;


@Controller
public class WebSocketCtrl {
    private final SimpMessagingTemplate template;
    private final NotificacionSrv notificacionSrv;
    private final AlertaSrv alertaSrv;

    public WebSocketCtrl(SimpMessagingTemplate template, UsuarioSrv usuarioSrv, AlertaSrv alertaSrv, NotificacionSrv notificacionSrv, NotificacionSrv notificacionSrv1, AlertaSrv alertaSrv1) {
        this.template = template;
        this.notificacionSrv = notificacionSrv1;
        this.alertaSrv = alertaSrv1;
    }

    @PostConstruct
    void init() {
        template.convertAndSend("/topic/point_update", Marker.builder().id("1").latitude(-78).longitude(-0.1).tipo("1").build());
    }

    @GetMapping("/test")
    public void test() throws InterruptedException {
        for (int i = 0; i < 50; i++) {
            for (int j = 0; j < 15; j++) {
                Notificacion notificacion = Notificacion.builder().idUsuario("" + j).latitude(-78.502958 + (((int) (Math.random() * 100) + 1) / 100000.0)).longitude(-0.198491 + (((int) (Math.random() * 100) + 1) / 100000.0)).fcAlerta(LocalDateTime.now()).build();
                notificacionSrv.add(notificacion);
            }
        }
    }

    @GetMapping("/test1")
    public void test1() throws InterruptedException {
        LocalDateTime fI = LocalDateTime.now();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 15; j++) {
                Alerta alerta = Alerta.builder()
                        .idUsuario("e" + j).latitude(-78.502958 + (((int) (Math.random() * 100) + 1) / 100000.0)).
                                longitude(-0.198491 + (((int) (Math.random() * 100) + 1) / 100000.0))
                        .fcInicio(fI)
                        .fcAlerta(LocalDateTime.now()).activo(true).build();
                alertaSrv.add(alerta);
            }
        }
        for (int j = 5; j < 15; j++) {
            Alerta alerta = Alerta.builder()
                    .idUsuario("e" + j).latitude(-78.502958 + (((int) (Math.random() * 100) + 1) / 100000.0)).
                            longitude(-0.198491 + (((int) (Math.random() * 100) + 1) / 100000.0))
                    .fcInicio(fI)
                    .fcFin(LocalDateTime.now())
                    .fcAlerta(LocalDateTime.now()).activo(false).build();
            alertaSrv.add(alerta);
        }

    }

    @GetMapping("/test2")
    public void test2() throws InterruptedException {
        LocalDateTime fI = LocalDateTime.now();
        for (int j = 10; j < 15; j++) {
            Alerta alerta = Alerta.builder()
                    .idUsuario("e" + j).latitude(-78.502958 + (((int) (Math.random() * 100) + 1) / 100000.0)).
                            longitude(-0.198491 + (((int) (Math.random() * 100) + 1) / 100000.0))
                    .fcInicio(fI)
                    .fcAlerta(LocalDateTime.now()).activo(true).build();
            alertaSrv.add(alerta);
            Thread.sleep(5000);
        }
    }

}
