package group.jedai.panicbtn.ctrl;

import com.fasterxml.jackson.databind.node.ObjectNode;
import group.jedai.panicbtn.dto.Notificacion;
import group.jedai.panicbtn.dto.Usuario;
import group.jedai.panicbtn.srv.NotificacionSrv;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@RestController
public class NotificacionCtrl {

    private final NotificacionSrv notificacionSrv;

    public NotificacionCtrl(NotificacionSrv notificacionSrv) {
        this.notificacionSrv = notificacionSrv;
    }

    @PostMapping(value = "/api/movil/notificacion", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Notificacion add(@RequestBody Notificacion notificacion) {
        System.out.println(notificacion);
        return notificacionSrv.add(notificacion);
    }

    @GetMapping("/api/notificacion")
    public void posicionActualG() {
        notificacionSrv.posicionActualG();
    }
}
