package group.jedai.panicbtn.ctrl;

import com.fasterxml.jackson.databind.node.ObjectNode;
import group.jedai.panicbtn.dto.Alerta;
import group.jedai.panicbtn.srv.AlertaSrv;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class AlertaCtrl {

    private final AlertaSrv alertaSrv;
    private final SimpMessagingTemplate template;

    public AlertaCtrl(AlertaSrv alertaSrv, SimpMessagingTemplate template) {
        this.alertaSrv = alertaSrv;
        this.template = template;
    }

    @GetMapping("/api/alerta")
    public List<Alerta> listAlert() {
        return alertaSrv.findAlertas();
    }

    @PostMapping(value = "/api/movil/alerta", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Alerta add(@RequestBody Alerta alerta) throws InterruptedException {
        var alert = alertaSrv.add(alerta);
        System.out.println("respuesta: " + alert);
        return alert;
    }

    @PostMapping("/api/alertas")
    public ResponseEntity<?> findAlertas(@RequestBody ObjectNode item) {
        try {
            Map<String, Object> res = alertaSrv.findByFc(item);
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/api/alerta")
    public ResponseEntity<?> findAlerta(@RequestBody ObjectNode item) {
        try {
            Map<String, Object> res = alertaSrv.alrtByFcIByIdUs(item);
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/api/resumen")
    public ResponseEntity<?> resumen() {
        Map<String, Object> res = alertaSrv.count();
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @MessageMapping("/ws")
    @SendTo("/topic/point_update")
    public Alerta test(Alerta message) {
        System.out.println(("MESSAGE: " + message.getId() + "; " + message.getFcAlerta() + "; " + message.getLatitude() + "; " + message.getLongitude()));
        return message;
    }

    @GetMapping("/test/alerta")
    public void test() {
        template.convertAndSend("/topic/test",
                Alerta.builder().id("12345678").idUsuario("14561").latitude(0.164155).longitude(-0.144574).build());
    }
}