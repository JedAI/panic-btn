package group.jedai.panicbtn.ctrl;

import group.jedai.panicbtn.dto.EstadoAlerta;
import group.jedai.panicbtn.dto.MotivoAlerta;
import group.jedai.panicbtn.srv.MotivoAlertaSrv;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MotivoAlertaCtrl {

    private final MotivoAlertaSrv motivoAlertaSrv;

    public MotivoAlertaCtrl(MotivoAlertaSrv motivoAlertaSrv) {
        this.motivoAlertaSrv = motivoAlertaSrv;
    }

    @PostMapping("/api/movil/motivoalerta")
    public MotivoAlerta save(@RequestBody MotivoAlerta item) {
        return motivoAlertaSrv.save(item);
    }

    @GetMapping("/api/movil/motivoalerta")
    public List<String> list() {
        return motivoAlertaSrv.list();
    }

}
