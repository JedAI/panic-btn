package group.jedai.panicbtn.ctrl;

import group.jedai.panicbtn.dto.EstadoAlerta;
import group.jedai.panicbtn.dto.NivelServicio;
import group.jedai.panicbtn.srv.EstadoAlertaSrv;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EstadoAlertaCtrl {

    private final EstadoAlertaSrv estadoAlertaSrv;

    public EstadoAlertaCtrl(EstadoAlertaSrv estadoAlertaSrv) {
        this.estadoAlertaSrv = estadoAlertaSrv;
    }

    @PostMapping("/api/movil/estadoalerta")
    public EstadoAlerta save(@RequestBody EstadoAlerta item) {
        return estadoAlertaSrv.save(item);
    }

    @GetMapping("/api/movil/estadoalerta")
    public List<String> list() {
        return estadoAlertaSrv.list();
    }

}
