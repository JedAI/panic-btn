package group.jedai.panicbtn.ctrl;

import com.fasterxml.jackson.databind.node.ObjectNode;
import group.jedai.panicbtn.dto.NivelServicio;
import group.jedai.panicbtn.srv.NivelServicioSrv;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class NivelServicioCtrl {

    private final NivelServicioSrv nivelServicioSrv;

    public NivelServicioCtrl(NivelServicioSrv nivelServicioSrv) {
        this.nivelServicioSrv = nivelServicioSrv;
    }

    @PostMapping("/api/movil/nivelservicio")
    public NivelServicio save(@RequestBody NivelServicio item) {
        return nivelServicioSrv.save(item);
    }

    @GetMapping(value = "/api/nivelservicio")
    public List<NivelServicio> list() {
        return nivelServicioSrv.list();
    }

    @GetMapping(value = "/api/nivelservicio", params = "idA")
    public NivelServicio findByIdA(String idA) {
        return nivelServicioSrv.findByIdA(idA);
    }

    @PostMapping("/api/nivelservicio")
    public ResponseEntity<?> resumen(@RequestBody ObjectNode item) {
        try {
            Map<String, Object> res = nivelServicioSrv.colsuta(item);
            return new ResponseEntity<>(res, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/api/nivelservicio", params = "id")
    public NivelServicio saveWeb(@RequestBody NivelServicio item) {
        return nivelServicioSrv.save(item);
    }

}
