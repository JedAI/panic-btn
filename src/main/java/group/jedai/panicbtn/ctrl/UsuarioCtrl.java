package group.jedai.panicbtn.ctrl;

import group.jedai.panicbtn.dto.Usuario;
import group.jedai.panicbtn.srv.UsuarioSrv;
import org.apache.camel.Produce;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import javax.activation.MimeType;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@RestController
public class UsuarioCtrl {

    private final UsuarioSrv usuarioSrv;

    @Value("${app.files.fotos}")
    private String FOTOS_FOLDER = "./";
    private String jpgFile = ".jpg";

    public UsuarioCtrl(UsuarioSrv usuarioSrv) {
        this.usuarioSrv = usuarioSrv;
    }

    /**
     * lista todos los usuarios
     *
     * @return lista usuarios
     */
    @GetMapping("/api/usuario")
    public List<Usuario> list() {
        return usuarioSrv.usuarioList();
    }

    /**
     * busca un usuario
     *
     * @return usuario
     */
    @GetMapping(value = "/api/usuario", params = "id", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Usuario findOne(@RequestParam String id) {
        return usuarioSrv.usuarioById(id);
    }

    /**
     * guarda a un nuevo usuario
     *
     * @param usuario usuario hacer regsitrado
     * @return
     */
    @PostMapping(value = "/api/movil/usuario", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Usuario add(@RequestBody Usuario usuario) {
        return usuarioSrv.add(usuario);
    }

    /**
     * Activar al usuario
     *
     * @param m mail del usuario
     * @param c clave de acceso
     */
    @GetMapping(value = "/api/movil/usuario/activar/{m}/{c}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Usuario activar(@PathVariable String m, @PathVariable String c) {
        return usuarioSrv.activar(m, c);
    }

    /**
     * buscar un usuario
     *
     * @param id id del usuario de busqueda
     * @return Usuario encontrado
     */
    @GetMapping(value = "/api/movil/usuario/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Usuario findById(@PathVariable String id) {
        return usuarioSrv.usuarioById(id);
    }

    /**
     * busca un uasuraio por su mail
     *
     * @param m mail del usuario
     * @return usuario encontrado
     */
    @GetMapping(value = "/api/movil/usuarioVerificar/{m}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Usuario findByMail(@PathVariable String m) {
        return usuarioSrv.findUserByMail(m);
    }

    @GetMapping(value = "/api/movil/usuario/{m}/{p}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Usuario findByLogin(@PathVariable String m, @PathVariable String p) {
        return usuarioSrv.findUserLogin(m, p);
    }


    /**
     * metodo para guardar la foto del usuario
     *
     * @param req recibe HttpServletRequest con la imagen y el id del usuario
     * @return
     * @throws IOException
     */
    @PostMapping(value = "/api/movil/usuario/foto", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String foto(HttpServletRequest req) throws IOException {

        byte[] data = ((StandardMultipartHttpServletRequest) req).getFile("foto").getBytes();
        String id = ((StandardMultipartHttpServletRequest) req).getParameter("id");
        return usuarioSrv.savePhoto(data, id);
    }

    /**
     * retorna la imagen de usuario requerida
     *
     * @param id id del usuario solicitante
     * @return url de la imagen retornada
     * @throws IOException
     */
    @RequestMapping(value = "/api/movil/foto/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public ResponseEntity<byte[]> dwnldFoto(@PathVariable("id") String id) throws IOException {
        Path path;
        File f = new File(FOTOS_FOLDER + id + jpgFile);
        if (f.exists()) {
            path = f.toPath();
        } else {
            path = new File(FOTOS_FOLDER + "foto.jpg").toPath();
        }
        byte[] imageContent = Files.readAllBytes(path);
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        System.out.println("entro al servidor a consultar la imagen");
        return new ResponseEntity<>(imageContent, headers, HttpStatus.OK);
    }

    /**
     * resetear clave de acceso mediante el mail
     *
     * @param mail registrado por el usuario previamente para su reseteo
     * @return
     */
    @GetMapping(value = "/api/movil/usuario/reenviar/{mail}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Usuario reenviarCod(@PathVariable String mail) {
        return usuarioSrv.resetClave(mail);
    }

    /**
     * resetear clave de acceso mediante el mail
     *
     * @param mail registrado por el usuario previamente para su reseteo
     * @return
     */
    @GetMapping(value = "/api/movil/usuario/resetPass/{mail}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Usuario resetPass(@PathVariable String mail) {
        return usuarioSrv.resetPass(mail);
    }

    @PostMapping(value = "/api/movil/usuario/cambiarPass", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String cambiarPass(@RequestBody List<String> user) {
        return usuarioSrv.cambiarPass(user);
    }

    @GetMapping("/api/usuario/tipo")
    public List<Usuario> usuariosG() {
        return usuarioSrv.usuarioListNotificacion();
    }

    @GetMapping("/api/usuario/encuesta/enviar")
    public void enviarEncuesta(@RequestParam boolean enviar) {
        usuarioSrv.enviarEncuesta(enviar);
    }

}

