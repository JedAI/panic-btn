package group.jedai.panicbtn.ctrl;

import group.jedai.panicbtn.dto.Usuario;
import group.jedai.panicbtn.srv.UsuarioSrv;
import group.jedai.panicbtn.util.SessionAttr;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class IndexCtrl {

    @Value("${app.login.admin.username}")
    String adminUsername;

    private final UsuarioSrv usuarioService;


    public IndexCtrl(UsuarioSrv usuarioService) {
        this.usuarioService = usuarioService;
    }

    @GetMapping("/enki")
    public Map<String, Object> jedai(HttpServletRequest request) {

        Map<String, Object> res = new HashMap<>();
        String username = request.getSession().getAttribute(SessionAttr.USROCDGO.getCode()).toString();
        Usuario admin = Usuario.builder().id(adminUsername).nombre("enki").tipo("ADMINISTRADOR").build();
        Usuario u = usuarioService.loadUserByUsername(username).orElse(admin);
        res.put("username", u.getMail());
        res.put("id", u.getId());
        res.put("tipo", u.getTipo());
        return res;
    }

}
