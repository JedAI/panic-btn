package group.jedai.panicbtn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PanicBtnApplication {

    public static void main(String[] args) {
        SpringApplication.run(PanicBtnApplication.class, args);
    }
}
