package group.jedai.panicbtn.dao;

import group.jedai.panicbtn.dto.EstadoAlerta;
import group.jedai.panicbtn.dto.NivelServicio;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstadoAlertaDAO extends MongoRepository<EstadoAlerta, String> {
    EstadoAlerta findByNm(String nm);
}
