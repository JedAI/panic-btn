package group.jedai.panicbtn.dao;

import group.jedai.panicbtn.dto.MotivoAlerta;
import group.jedai.panicbtn.dto.NivelServicio;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MotivoAlertaDAO extends MongoRepository<MotivoAlerta, String> {
    MotivoAlerta findByNm(String nm);
}
