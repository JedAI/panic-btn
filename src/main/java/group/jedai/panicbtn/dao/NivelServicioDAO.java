package group.jedai.panicbtn.dao;

import group.jedai.panicbtn.dto.Alerta;
import group.jedai.panicbtn.dto.NivelServicio;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NivelServicioDAO extends MongoRepository<NivelServicio, String> {

    List<NivelServicio> findAllByM(String motivo);

    List<NivelServicio> findAllByNa(String niv);

    NivelServicio findByIdA(String idA);
}
