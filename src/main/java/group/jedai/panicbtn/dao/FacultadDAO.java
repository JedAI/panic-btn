package group.jedai.panicbtn.dao;

import group.jedai.panicbtn.dto.Facultad;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacultadDAO extends MongoRepository<Facultad, String> {

}
