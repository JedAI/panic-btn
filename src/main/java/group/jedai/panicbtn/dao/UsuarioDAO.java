package group.jedai.panicbtn.dao;

import group.jedai.panicbtn.dto.Usuario;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioDAO extends MongoRepository<Usuario, String> {

    Usuario findByMail(String mail);

    Usuario findByMailAndPassword(String mail, String password);

    Usuario findByMailAndClaveAcceso(String mail, String claveAcceso);

    Usuario findByIdAndAsig(String id, boolean asig);

    List<Usuario> findByTipo(String tipo);

    List<Usuario> findByTipoIsNot(String tipo);

}
