package group.jedai.panicbtn.dao;

import group.jedai.panicbtn.dto.Notificacion;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificacionDAO extends MongoRepository<Notificacion, String> {

}
