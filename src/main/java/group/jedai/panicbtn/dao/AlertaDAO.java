package group.jedai.panicbtn.dao;

import group.jedai.panicbtn.dto.Alerta;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AlertaDAO extends MongoRepository<Alerta, String> {

    Alerta findByIdUsuarioAndActivo(String idUsuario, boolean activo);

}
