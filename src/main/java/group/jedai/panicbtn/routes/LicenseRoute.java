package group.jedai.panicbtn.routes;

import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.stereotype.Component;

/**
 * @author dacopan on 14/12/18
 */
@Component
public class LicenseRoute extends SpringRouteBuilder {

    @Override
    public void configure() throws Exception {

        from("quartz2://rm/cronSendRenewal?cron=0+0/5+*+?+*+*+*")
                .to("bean:notificacionSrv?method=posicionActualG");


    }
}
