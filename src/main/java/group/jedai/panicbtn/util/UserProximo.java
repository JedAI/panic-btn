package group.jedai.panicbtn.util;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserProximo implements Comparable<UserProximo> {
    private String id;
    private double dist;

    @Override
    public int compareTo(UserProximo o) {
        if (dist < o.dist) {
            return -1;
        }
        if (dist > o.dist) {
            return 1;
        }
        return 0;
    }
}
