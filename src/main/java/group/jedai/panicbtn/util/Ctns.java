package group.jedai.panicbtn.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Ctns {

    @Autowired
    public Ctns(@Value("${app.login.intentos}") Integer maxLoginFail,
                @Value("${app.login.retry-time}") Integer loginRetryTime) {
        MAX_LOGIN_FAIL = maxLoginFail;
        LOGIN_RETRY_TIME = loginRetryTime;
    }

    //static properties
    public static Integer MAX_LOGIN_FAIL = 3;
    public static Integer LOGIN_RETRY_TIME = 3;

    //pages
    public static final String LOGIN_SUCCESS_URL = "/";
    public static final String LOGIN_URL = "/login";
    public static final String EXPIRED_URL = "/faces/v/p/error.xhtml";
    public static final String DENIED_URL = "/faces/v/p/denied.xhtml";


}