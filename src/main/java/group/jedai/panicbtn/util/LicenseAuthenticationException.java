package group.jedai.panicbtn.util;

import org.springframework.security.core.AuthenticationException;

public class LicenseAuthenticationException extends AuthenticationException {

    public LicenseAuthenticationException(String msg) {
        super(msg);
    }
}
