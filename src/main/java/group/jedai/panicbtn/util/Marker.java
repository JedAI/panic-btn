package group.jedai.panicbtn.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author dacopanCM on 14/10/18.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Marker {
    private String id;
    private String nm;
    private String nmG;
    private String idG;
    private String tipo;
    private double latitude;
    private double longitude;
}
