package group.jedai.panicbtn.util;

public enum SessionAttr {

    USROCDGO("USROCDGO"),
    PERMISSION("permiso");

    private String code;

    private SessionAttr(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}

