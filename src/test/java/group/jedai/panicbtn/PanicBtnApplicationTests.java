package group.jedai.panicbtn;

import group.jedai.panicbtn.dto.*;
import group.jedai.panicbtn.srv.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PanicBtnApplicationTests {

    @Autowired
    EstadoAlertaSrv estadoAlertaSrv;

    @Autowired
    MotivoAlertaSrv motivoAlertaSrv;

    @Test
    public void contextLoads() {
    }

    @Test
    public void agregarMotivos() {
        MotivoAlerta motivoAlerta = new MotivoAlerta();
        motivoAlerta.setId(null);
        motivoAlerta.setNm("Robo");
        motivoAlertaSrv.save(motivoAlerta);

        motivoAlerta.setId(null);
        motivoAlerta.setNm("Acoso");
        motivoAlertaSrv.save(motivoAlerta);

        motivoAlerta.setId(null);
        motivoAlerta.setNm("Error");
        motivoAlertaSrv.save(motivoAlerta);

        motivoAlerta.setId(null);
        motivoAlerta.setNm("Otro");
        motivoAlertaSrv.save(motivoAlerta);

        EstadoAlerta estadoAlerta = new EstadoAlerta();
        estadoAlerta.setId(null);
        estadoAlerta.setNm("Atendida");
        estadoAlertaSrv.save(estadoAlerta);

        estadoAlerta.setId(null);
        estadoAlerta.setNm("No atendida");
        estadoAlertaSrv.save(estadoAlerta);

        estadoAlerta.setId(null);
        estadoAlerta.setNm("Guardia tardo mucho");
        estadoAlertaSrv.save(estadoAlerta);

        estadoAlerta.setId(null);
        estadoAlerta.setNm("Guardia nunca llegó");
        estadoAlertaSrv.save(estadoAlerta);

    }
}
