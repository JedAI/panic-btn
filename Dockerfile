FROM openjdk:11-jdk-oraclelinux7
LABEL maintainer=alexcoropilaguano@hotmail.com
COPY ./build/libs/panic-btn-1.jar /home/panic-btn-1.jar
EXPOSE 8080
ENV spring.profiles.active=prod
CMD java -server -Duser.country=EC -Duser.language=es -Duser.timezone=America/Guayaquil -jar /home/panic-btn-1.jar