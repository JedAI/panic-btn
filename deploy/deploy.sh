#!/usr/bin/env bash
###################################  CONSTANTES  ###################################
DATE=$(date +%Y-%m-%d)
LINE="--------------------------------------------------------------------------"

# COLORES
LBLUE="\033[1;34m"  #1
LGREEN="\033[1;32m" #2
LRED="\033[1;31m"   #3
LCYAN="\033[1;36m"  #4
NC="\033[0m"        #No Color



###################################  FUNCIONES  ###################################
# Muestra mensajes con colores
function text_color {
    local text=$2
    if [[ $1 -eq 1 ]]; then
        text="${LBLUE}$2${NC}"
    elif [[ $1 -eq 2 ]]; then
        text="${LGREEN}$2${NC}"
    elif [[ $1 -eq 3 ]]; then
        text="${LRED}$2${NC}"
    elif [[ $1 -eq 4 ]]; then
        text="${LCYAN}$2${NC}"
    fi
    echo "$text"
}

function cecho {
    if [[ $# -gt 0 ]] && [[ $(( $# % 2 )) -eq 0 ]]; then
        local text=""
        while (( "$#" )); do
            num=$1
            shift
            local color=$(text_color num "$1")
            text="${text}${color}"
            shift
        done
        echo -e "${text}"
    fi
}

function check_success {
    if [[ $1 -eq 0 ]]; then
        cecho 2 "OK"
    else
        cecho 3 "ERROR"
        exit 1
    fi
}

#####################################  MAIN  #####################################

cecho 1 "Iniciando sesion en Gitlab Registry"
sudo docker login registry.gitlab.com -u smartalertuce -p 65UXF7RTdgxdyM5hq_sM
echo

cecho 1 "Ejecutando el docker-compose"
sudo docker-compose -f "docker-compose.yml" pull && sudo docker-compose -f "docker-compose.yml" up -d